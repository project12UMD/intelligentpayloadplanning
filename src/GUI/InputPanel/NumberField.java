package GUI.InputPanel;

import javafx.scene.control.TextField;

/**
 * This extends text field to only accept numbers of so many digits
 * does not accept non numeric values.
 */
public class NumberField extends TextField
{
    private int limit;

    /**
     * Constructor that creates a NumberField that accepts only a number of digits
     *
     * @param limit
     *         number of digits to accept
     */
    NumberField(int limit)
    {
        this.limit = limit;
    }

    /**
     * Method that replaces range of characters with the given text
     *
     * @param start
     *         start index for the range, inclusive
     * @param end
     *         end index for the range, exclusive.
     * @param text
     *         Non null text to replace the range.
     */
    @Override
    public void replaceText(int start, int end, String text)
    {
        if(validate(text))
        {
            super.replaceText(start, end, text);
        }

        restrictLength();
    }

    /**
     * Method that replaces the selection with the given replacement string
     *
     * @param text
     *         non null text to replace the selection
     */
    @Override
    public void replaceSelection(String text)
    {
        if(validate(text))
        {
            super.replaceSelection(text);
        }

        restrictLength();
    }

    /**
     * Makes sure that the text is numeric
     *
     * @param text
     *         text to validate
     *
     * @return true if valid, invalid if false
     */
    private boolean validate(String text)
    {
        return text.matches("\\d*");
    }

    /**
     * Restrict the length to 'limit' digits
     */
    private void restrictLength()
    {
        if(this.getText()
               .length() > limit)
        {
            this.setText(this.getText()
                             .substring(0, limit));
            this.positionCaret(limit);
        }
    }
}
