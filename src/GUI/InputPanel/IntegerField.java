package GUI.InputPanel;

import javafx.application.Platform;
import javafx.scene.control.TextField;

/**
 * Field of Text
 * Integers going in and Values that are set to Default
 */
public class IntegerField extends TextField
{
    private Integer min;
    private Integer max;
    private Integer defaultValue;

    /**
     * @param min
     *          min value
     * @param max
     *          max value
     * @param defaultValue
     *          default value
     */
    IntegerField(Integer min, Integer max, Integer defaultValue)
    {
        this.min = min;
        this.max = max;
        this.defaultValue = defaultValue;

        this.setText("" + defaultValue);
        this.textProperty().addListener((o, oldValue, newValue) ->
        {
            if (!newValue.matches("\\d*"))
            {
                this.setText(oldValue);
            }
            else if (newValue.isEmpty())
            {
                this.setText(oldValue);
                this.requestFocus();
                Platform.runLater(this::selectAll);
            }
            else
            {
                int value = Integer.parseInt(this.getText());
                value = Utils.restrict(value, min, max);
                this.setText("" + value);
            }
        });
    }

    /**
     * @return min gets the min value
     */
    public Integer getMin()
    {
        return min;
    }

    /**
     * @param min  sets Minimum integer possible in field
     */
    public void setMin(Integer min)
    {
        this.min = min;
    }

    /**
     * @return max gets the max value
     */
    public Integer getMax()
    {
        return max;
    }

    /**
     * @param max sets Max integer possible in the field
     */
    public void setMax(Integer max)
    {
        this.max = max;
    }

    /**
     * @return defaultValue gets the default value of the field
     */
    public Integer getDefaultValue()
    {
        return defaultValue;
    }

    /**
     * @param defaultValue Default values set in
     */
    public void setDefaultValue(Integer defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    /**
     * Reset Button - Resets the Input Panel to default values
     */
    public void reset()
    {
        this.setText("" + defaultValue);
    }
}
