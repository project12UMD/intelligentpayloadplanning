package GUI.InputPanel;

import DataHandlers.Constraint.CellStatuses;
import DataHandlers.Constraint.Constraint;
import GUI.GanttChart.GanttChart;
import javafx.geometry.HPos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

import java.util.function.Supplier;

/**
 * Main Input GUI Panel - User Constraints - manual inputs
 * 1. Number of Tubes [ Max of 20 ]
 *      - Number of Simultaneous tubes [ Max of 20 or less ]
 * 2. Number of Cells [ Max of 10 ]
 *      - Number of Simultaneous Cells [ Max of 10 or less ]
 * 3. Hatch Time
 *      - Time it take for the Hatch to open and close
 *      - Between [ 1 - 3600 Seconds]
 * 4. Deployment Time
 *      - Time it takes for the payload to deploy to its location
 *      - Between [ 1 - 3600 seconds ]
 * 5. Locations
 *  Time (sec)
 *      - Time you want it to take for 1 payload to reach its destination
 *      - Between [ 1 - 3600 seconds ]
 *  Payloads
 *      - Amount of Missiles you would like to send to a certain location
 *      - Max of  [ 1 - 20 ]
 *
 */
public class InputPanel extends BorderPane //TODO fix error w
{
    private UpdateConsumer action;
    private Supplier<CellStatuses> statusSupplier;
    private Runnable changeHandler;

    private ScrollPane scroller;

    private IntegerSlider numTubesSlider, numSimTubesSlider;
    private IntegerSlider numCellsSlider, numSimCellsSlider;

    private Label numTubesLabel, numSimTubesLabel;
    private Label numCellsLabel, numSimCellsLabel;

    private IntegerField hatchTimeField, deployTimeField;

    private VBox locationsVBox;
    private Button addLocationButton, removeLocationButton;

    /**
     * @param action         Actions that specified, such as payload, time, cells, tubes.
     * @param statusSupplier statusSupplier supplies the current status of the give field..
     */
    public InputPanel(UpdateConsumer action, Supplier<CellStatuses> statusSupplier)
    {
        this.action = action;
        this.statusSupplier = statusSupplier;
        initComponents();
        initActions();
    }


    /**
     * @return builder.build(statusSupplier)
     * after building the constraints from the inputs set in the input panel, this method will get values that are put in.
     */
    public Constraint getConstraints()
    {
        Constraint.Builder builder = new Constraint.Builder()
                .setDeploymentTime(Integer.parseInt(deployTimeField.getText()))
                .setHatchTime(Integer.parseInt(hatchTimeField.getText()))
                .setDeploymentLimit(numSimCellsSlider.getValue())
                .setHatchLimit(numSimTubesSlider.getValue())
                .setCellAmount(numCellsSlider.getValue())
                .setTubeAmount(numTubesSlider.getValue());

        for (int i = 0; i < locationsVBox.getChildren().size(); i++)
        {
            HBox row = (HBox) locationsVBox.getChildren().get(i);
            IntegerField timeField = (IntegerField) row.getChildren().get(1);
            IntegerSpinner payloadSpinner = (IntegerSpinner) row.getChildren().get(2);
            int time = Integer.parseInt(timeField.getText());
            int payloadAmount = payloadSpinner.getValue();
            builder.addLocation(payloadAmount, time);
        }

        return builder.build(statusSupplier.get());
    }

    /**
     * Call an update to the Number of Tubes Sliders.
     * - slide the slider to set a value
     */
    public void callUpdate()
    {
        action.update(numTubesSlider.getValue(), numCellsSlider.getValue());
    }

    /**
     * @param handler Runnable Handler gets set
     */
    public void setChangeHandler(Runnable handler)
    {
        changeHandler = handler;
    }

    /**
     * Initial Components
     * Components of the Input panel -
     * Number of tubes - simultaneous tubes - number of cells - simultaneous cells
     *
     */
    private void initComponents()
    {
        final VBox constraintsVBox = new VBox();
        constraintsVBox.getStyleClass().add("constraints-vbox");

        final Label numTubesHeaderLabel = new Label("Number of Tubes:");
        numTubesHeaderLabel.getStyleClass().add("dark-header-label");
        numTubesHeaderLabel.setFont(Font.font(20));
        numTubesSlider = new IntegerSlider(1, 20, 1, "tube");
        numTubesLabel = new Label("1");
        numTubesLabel.getStyleClass().add("dark-box-label");

        final Label numSimTubesHeaderLabel = new Label("Simultaneous Tubes:");
        numSimTubesHeaderLabel.getStyleClass().add("dark-header-label");
        numSimTubesHeaderLabel.setFont(Font.font(20));
        numSimTubesSlider = new IntegerSlider(1, 20, 1, "tube");
        numSimTubesLabel = new Label("1");
        numSimTubesLabel.getStyleClass().add("dark-box-label");

        final Label numCellsHeaderLabel = new Label("Number of Cells:");
        numCellsHeaderLabel.getStyleClass().add("dark-header-label");
        numCellsHeaderLabel.setFont(Font.font(20));
        numCellsSlider = new IntegerSlider(1, 10, 1, "cell");
        numCellsLabel = new Label("1");
        numCellsLabel.getStyleClass().add("dark-box-label");

        final Label numSimCellsHeaderLabel = new Label("Simultaneous Cells:");
        numSimCellsHeaderLabel.getStyleClass().add("dark-header-label");
        numSimCellsHeaderLabel.setFont(Font.font(20));
        numSimCellsSlider = new IntegerSlider(1, 10, 1, "cell");
        numSimCellsLabel = new Label("1");
        numSimCellsLabel.getStyleClass().add("dark-box-label");

        final Label hatchTimeLabel = new Label("Hatch Time:");
        hatchTimeLabel.getStyleClass().add("dark-header-label");
        hatchTimeLabel.setFont(Font.font(20));
        hatchTimeField = new IntegerField(1, 3600, 3);
        hatchTimeField.getStyleClass().add("dark-text-field");

        final Label deployTimeLabel = new Label("Deployment Time:");
        deployTimeLabel.getStyleClass().add("dark-header-label");
        deployTimeLabel.setFont(Font.font(20));
        deployTimeField = new IntegerField(1, 3600, 3);
        deployTimeField.getStyleClass().add("dark-text-field");

        GridPane constraintGrid = new GridPane();
        constraintGrid.getStyleClass().add("constraint-grid");
        constraintGrid.add(numTubesHeaderLabel, 0, 0, 1, 1);
        constraintGrid.add(numTubesSlider, 0, 1, 1, 1);
        constraintGrid.add(numTubesLabel, 1, 0, 1, 2);
        constraintGrid.add(numSimTubesHeaderLabel, 0, 2, 1, 1);
        constraintGrid.add(numSimTubesSlider, 0, 3, 1, 1);
        constraintGrid.add(numSimTubesLabel, 1, 2, 1, 2);
        constraintGrid.add(numCellsHeaderLabel, 0, 4, 1, 1);
        constraintGrid.add(numCellsSlider, 0, 5, 1, 1);
        constraintGrid.add(numCellsLabel, 1, 4, 1, 2);
        constraintGrid.add(numSimCellsHeaderLabel, 0, 6, 1, 1);
        constraintGrid.add(numSimCellsSlider, 0, 7, 1, 1);
        constraintGrid.add(numSimCellsLabel, 1, 6, 1, 2);
        constraintGrid.add(hatchTimeLabel, 0, 8, 2, 1);
        constraintGrid.add(hatchTimeField, 0, 9, 2, 1);
        constraintGrid.add(deployTimeLabel, 0, 10, 2, 1);
        constraintGrid.add(deployTimeField, 0, 11, 2, 1);
        GridPane.setHalignment(numTubesLabel, HPos.RIGHT);
        GridPane.setHalignment(numSimTubesLabel, HPos.RIGHT);
        GridPane.setHalignment(numCellsLabel, HPos.RIGHT);
        GridPane.setHalignment(numSimCellsLabel, HPos.RIGHT);

        final Label locationsLabel = new Label("Locations:");
        locationsLabel.getStyleClass().add("dark-header-label");

        constraintsVBox.getChildren().add(constraintGrid);

        final Label fillerLabel = new Label();
        fillerLabel.getStyleClass().add("dark-location-label");

        final Label amountLabel = new Label("Time (sec)");
        amountLabel.getStyleClass().add("dark-location-header-label");
        amountLabel.setFont(Font.font(15));

        final Label timeLabel = new Label("Payloads");
        timeLabel.setFont(Font.font(15));
        timeLabel.getStyleClass().add("dark-location-header-label");

        final HBox locationListHeader = new HBox();
        locationListHeader.getStyleClass().add("dark-location-hbox");

        locationListHeader.getChildren().addAll(
                fillerLabel,
                amountLabel,
                timeLabel
        );

        locationsVBox = new VBox();
        locationsVBox.getStyleClass().add("dark-location-vbox");
        for (int i = 0; i < 1; i++) addLocation();

        final VBox locationsGroupVBox = new VBox();
        locationsGroupVBox.getChildren().addAll(locationListHeader, locationsVBox);

        constraintsVBox.getChildren().add(locationsGroupVBox);

        scroller = new ScrollPane(constraintsVBox);
        scroller.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scroller.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

        addLocationButton = new Button("Add Location");
        addLocationButton.setPrefWidth(1000);
        addLocationButton.getStyleClass().add("dark-add-location-button");

        removeLocationButton = new Button("Remove Location");
        removeLocationButton.setPrefWidth(1000);
        removeLocationButton.getStyleClass().add("dark-remove-location-button");

        final GridPane buttonGrid = new GridPane();
        buttonGrid.add(addLocationButton, 0, 0);
        buttonGrid.add(removeLocationButton, 1, 0);

        ColumnConstraints half = new ColumnConstraints();
        half.setPercentWidth(50);
        buttonGrid.getColumnConstraints().addAll(half, half);

        this.setBottom(buttonGrid);
        this.setCenter(scroller);
        this.getStyleClass().add("root");
        this.getStylesheets().add("global.css");
    }

    /**
     * Initial Actions
     * Values are initially set on the Input Panel for intial start up and Reset Input Panel
     */
    private void initActions()
    {
        numTubesSlider.valueProperty().addListener((o, oldValue, newValue) ->
        {
            numTubesLabel.setText("" + newValue);
            int maxTubes = numTubesSlider.getValue();
            numSimTubesSlider.restrictUpper(maxTubes);
            action.update(maxTubes, numCellsSlider.getValue());
            if (changeHandler != null)
            {
                changeHandler.run();
            }
        });

        int maxTubes = numTubesSlider.getValue();
        numSimTubesSlider.restrictUpper(maxTubes);
        numSimTubesSlider.valueProperty().addListener((o, oldValue, newValue) ->
        {
            numSimTubesLabel.setText("" + newValue);
        });

        numCellsSlider.valueProperty().addListener((o, oldValue, newValue) ->
        {
            numCellsLabel.setText("" + newValue);
            int maxCells = numCellsSlider.getValue();
            numSimCellsSlider.restrictUpper(maxCells);
            action.update(numTubesSlider.getValue(), maxCells);
            if (changeHandler != null)
            {
                changeHandler.run();
            }
        });

        int maxCells = numCellsSlider.getValue();
        numSimCellsSlider.restrictUpper(maxCells);
        numSimCellsSlider.valueProperty().addListener((o, oldValue, newValue) ->
        {
            numSimCellsLabel.setText("" + newValue);
        });

        action.update(maxTubes, maxCells);

        locationsVBox.heightProperty().addListener(o ->
        {
            scroller.layout();
            scroller.setVvalue(1.0);
        });

        addLocationButton.setOnAction(e -> addLocation());
        removeLocationButton.setOnAction(e -> removeLocation());
    }

    /**
     * Adds a Location button to add a set location to where payloads are sent to.
     */
    private void addLocation()
    {
        int numConstraints = locationsVBox.getChildren().size();
        Label label = new Label(String.format("%3d.", numConstraints + 1));
        label.getStyleClass().add("dark-location-label");

        IntegerSpinner spinner = new IntegerSpinner(1, 20, 4);
        spinner.getStyleClass().add("dark-location-field");

        IntegerField timeField = new IntegerField(1, 3600, 3);
        timeField.getStyleClass().add("dark-location-field");

        HBox row = new HBox();
        row.getStyleClass().add("dark-location-hbox");
        row.getChildren().addAll(
                label,
                timeField,
                spinner
        );
        locationsVBox.getChildren().add(row);
    }

    /**
     * Removes the Location button to remove a set location to where payloads are sent to.
     */
    private void removeLocation()
    {
        int lastIndex = locationsVBox.getChildren().size() - 1;
        if (lastIndex > 0)
        {
            locationsVBox.getChildren().remove(lastIndex);
        }
    }

}
