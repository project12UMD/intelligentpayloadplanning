package GUI.InputPanel;

import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * creates a Dark theme to the Sliders field
 */
public class DarkSlider extends VBox
{
    private Slider slider;
    private ProgressBar sliderProgressBar;
    private Label sliderLabel;

    private String name;
    private int min;
    private int max;
    private int defaultValue;
    private String unitSingular;
    private String unitPlural;

    private ReadOnlyIntegerWrapper valueProperty;

    /**
     * @param name
     *         TODO Add documentation.
     * @param min
     *         TODO Add documentation.
     * @param max
     *         TODO Add documentation.
     * @param defaultValue
     *         TODO Add documentation.
     * @param unitSingular
     *         TODO Add documentation.
     * @param unitPlural
     *         TODO Add documentation.
     */
    DarkSlider(String name, int min, int max, int defaultValue,
               String unitSingular, String unitPlural)
    {
        this.name = name;
        this.min = min;
        this.max = max;
        this.defaultValue = defaultValue;
        this.unitSingular = unitSingular;
        this.unitPlural = unitPlural;

        valueProperty = new ReadOnlyIntegerWrapper();

        initComponents();
        initActions();
    }

    /**
     *  @return min
     *          returns minimum value
     */
    public int getMin()
    {
        return min;
    }

    /**
     * @return max
     *          returns the max value
     */
    public int getMax()
    {
        return max;
    }

    /**
     * @param lower
     *          restricts the lowest value possible, making it still a valid input
     */
    public void restrictLower(int lower)
    {
        min = lower;
        if(slider.getValue() < lower)
        {
            slider.setValue(lower);
        }
    }

    /**
     * @return valueProperty
     *          returns the value in the given property field
     */
    ReadOnlyIntegerProperty valueProperty()
    {
        return valueProperty.getReadOnlyProperty();
    }

    /**
     *@return valueProperty
     *          gets value in the given property field
     */
    int getValue()
    {
        return valueProperty.getValue();
    }

    /**
     * @param value
     *         value that is set into given property field.
     */
    public void setValue(int value)
    {
        valueProperty.setValue(value);
    }

    /**
     * @param upper
     *         restrict the upper value to be out of bound
     */
    void restrictUpper(int upper)
    {
        max = upper;
        if(slider.getValue() > upper)
        {
            slider.setValue(upper);
        }
    }

    /**
     * Initial Components
     * Components of the Input panel -
     * Number of tubes - simultaneous tubes - number of cells - simultaneous cells
     * sets min and max values, with a progress bar on the side - to display number on slider
     */
    private void initComponents()
    {
        final Label nameLabel = new Label(name);
        nameLabel.getStyleClass()
                 .add("dark-slider-name-label");

        slider = new Slider(min, max, defaultValue);
        slider.setBlockIncrement(1);
        slider.setMajorTickUnit(1);
        slider.setMinorTickCount(0);
        slider.setSnapToTicks(true);
        slider.getStyleClass()
              .add("dark-slider");

        sliderProgressBar = new ProgressBar(0);
        sliderProgressBar.getStyleClass()
                         .add("dark-slider-progress-bar");

        sliderLabel = new Label();
        sliderLabel.getStyleClass()
                   .add("dark-slider-label");
        updateInfo();

        final StackPane sliderStack = new StackPane();
        sliderStack.getChildren()
                   .addAll(sliderProgressBar, slider);

        final HBox sliderHBox = new HBox();
        sliderHBox.getChildren()
                  .addAll(sliderStack, sliderLabel);
        sliderHBox.getStyleClass()
                  .add("dark-slider-hbox");

        this.getChildren()
            .addAll(nameLabel, sliderHBox);
        this.getStyleClass()
            .add("dark-slider-vbox");
    }

    /**
     * Initial Actions
     * Values are initially set on the sliders for Tubes/sim Tubes/Cells/sim Cells
     */
    private void initActions()
    {
        slider.valueProperty()
              .addListener((o, oldValue, newValue) ->
                           {
                               if(slider.getValue() > max)
                               {
                                   slider.setValue(max);
                               }
                               else if(slider.getValue() < min)
                               {
                                   slider.setValue(min);
                               }
                               this.updateInfo();
                           });
    }

    /**
     * update the fields
     */
    private void updateInfo()
    {
        double range = slider.getMax() - slider.getMin();
        double progress = (slider.getValue() - slider.getMin()) / range;
        sliderProgressBar.setProgress(progress);

        int value = (int) Math.round(slider.getValue());
        String unitText = (value == 1) ? unitSingular : unitPlural;
        sliderLabel.setText(String.format("%d %s", value, unitText));

        valueProperty.setValue(value);
    }
}
