package GUI.InputPanel;

import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.layout.StackPane;

/**
 * Slider Bar on Values of Tubes, simultaneous tubes, Cells, simultaneous cells
 */
public class IntegerSlider extends StackPane
{
    private Slider slider;
    private ProgressBar sliderProgressBar;
    private Label unitLabel;

    private int min;
    private int max;
    private int defaultValue;
    private String unit;

    private ReadOnlyIntegerWrapper valueProperty;

    /**
     * @param min          min value
     * @param max          max value.
     * @param defaultValue default value
     * @param unit         String unit
     */
    IntegerSlider(int min, int max, int defaultValue, String unit)
    {
        this.min = min;
        this.max = max;
        this.defaultValue = defaultValue;
        this.unit = unit;

        valueProperty = new ReadOnlyIntegerWrapper();

        initComponents();
        initActions();
    }

    /**
     * @return min
     *          returns minimum value
     */
    public int getMin()
    {
        return min;
    }

    /**
     * @return max
     *          returns the max value
     */
    public int getMax()
    {
        return max;
    }

    /**
     * @param lower
     *          restricts the lowest value possible, making it still a valid input
     */
    public void restrictLower(int lower)
    {
        min = lower;
        if (slider.getValue() < lower)
        {
            slider.setValue(lower);
        }
    }

    /**
     * @return valueProperty
     *          returns the value in the given property field
     */
    ReadOnlyIntegerProperty valueProperty()
    {
        return valueProperty.getReadOnlyProperty();
    }

    /**
     * @return valueProperty
     *          gets value in the given property field
     */
    int getValue()
    {
        return valueProperty.getValue();
    }

    /**
     * @param value
     *          value that is set into given property field
     */
    public void setValue(int value)
    {
        valueProperty.setValue(value);
    }

    /**
     * @param upper
     *         restrict the upper value to be out of bound
     */
    void restrictUpper(int upper)
    {
        max = upper;
        if (slider.getValue() > upper)
        {
            slider.setValue(upper);
        }
    }

    /**
     * Initial Components
     * Components of the Input panel -
     * Number of tubes - simultaneous tubes - number of cells - simultaneous cells
     * sets min and max values, with a progress bar on the side - to display number on slider
     */
    private void initComponents()
    {
        slider = new Slider(min, max, defaultValue);
        slider.setBlockIncrement(1);
        slider.setMajorTickUnit(1);
        slider.setMinorTickCount(0);
        slider.setSnapToTicks(true);
        slider.getStyleClass().add("dark-slider");

        sliderProgressBar = new ProgressBar(0);
        sliderProgressBar.getStyleClass().add("dark-slider-progress-bar");

        unitLabel = new Label();
        unitLabel.getStyleClass().add("dark-slider-label");
        updateInfo();

        this.getChildren().addAll(sliderProgressBar, slider);
        this.getStyleClass().add("dark-slider-stack");
    }

    /**
     * Initial Actions
     * Values are initially set on the sliders for Tubes/sim Tubes/Cells/sim Cells
     */
    private void initActions()
    {
        slider.valueProperty().addListener((o, oldValue, newValue) ->
        {
            if (slider.getValue() > max)
            {
                slider.setValue(max);
            }
            else if (slider.getValue() < min)
            {
                slider.setValue(min);
            }
            this.updateInfo();
        });
    }

    /**
     * Updates the progress #, as the slider moves from right to left, on Tubes and Cells
     */
    private void updateInfo()
    {
        double range = slider.getMax() - slider.getMin();
        double progress = (slider.getValue() - slider.getMin()) / range;
        sliderProgressBar.setProgress(progress);

        int value = (int) Math.round(slider.getValue());
        String unitText = (value == 1) ? unit : unit + "s";
        unitLabel.setText(String.format("%d %s", value, unitText));

        valueProperty.setValue(value);
    }
}
