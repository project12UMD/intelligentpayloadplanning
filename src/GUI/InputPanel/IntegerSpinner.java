package GUI.InputPanel;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.util.StringConverter;

/**
 * Integer spinner java class - Spinner in the field to get the inputs from user, that are nor false value
 */
public class IntegerSpinner extends Spinner<Integer>
{
    private Integer min;
    private Integer max;
    private Integer defaultValue;

    /**
     * @param min min value
     * @param max max value
     * @param defaultValue default value
     */
    public IntegerSpinner(Integer min, Integer max, Integer defaultValue)
    {
        this.min = min;
        this.max = max;
        this.defaultValue = defaultValue;

        this.setEditable(true);
        this.setValueFactory(new SpinnerValueFactory
                .IntegerSpinnerValueFactory(min, max, defaultValue));
        this.getStyleClass().add(Spinner.STYLE_CLASS_SPLIT_ARROWS_HORIZONTAL);

        TextField spinnerField = this.getEditor();
        spinnerField.textProperty().addListener((o, oldValue, newValue) ->
        {
            if (!newValue.matches("\\d*"))
            {
                spinnerField.setText(oldValue);
            }
            else if (newValue.isEmpty())
            {
                spinnerField.setText(oldValue);
                spinnerField.requestFocus();
                Platform.runLater(spinnerField::selectAll);
            }
            else
            {
                StringConverter<Integer> converter
                        = this.getValueFactory().getConverter();
                int value = converter.fromString(spinnerField.getText());
                value = Utils.restrict(value, min, max);
                this.getValueFactory().setValue(value);
                spinnerField.setText("" + value);
            }
        });

        this.addEventFilter(MouseEvent.MOUSE_PRESSED, (EventHandler<MouseEvent>) e ->
        {
            ObservableList<String> styleClass
                    = e.getPickResult().getIntersectedNode().getStyleClass();
            if (styleClass.contains("increment-arrow-button")
                    || styleClass.contains("decrement-arrow-button"))
            {
                spinnerField.requestFocus();
                Platform.runLater(() ->
                {
                    spinnerField.positionCaret(spinnerField.getLength());
                });
            }
        });
    }

    /**
     * @return min gets the min value
     */
    public Integer getMin()
    {
        return min;
    }

    /**
     * @param min sets Minimum integer possible in field
     */
    public void setMin(Integer min)
    {
        this.min = min;
    }

    /**
     * @return max gets the max value
     */
    public Integer getMax()
    {
        return max;
    }

    /**
     * @param max sets Max integer possible in the field
     */
    public void setMax(Integer max)
    {
        this.max = max;
    }

    /**
     * @return defaultValue gets the default value of the field
     */
    public Integer getDefaultValue()
    {
        return defaultValue;
    }

    /**
     * @param defaultValue Default values set in
     */
    public void setDefaultValue(Integer defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    /**
     * Reset Button - Resets the Input Panel to default values
     */
    public void reset()
    {
        this.getValueFactory().setValue(defaultValue);
    }
}
