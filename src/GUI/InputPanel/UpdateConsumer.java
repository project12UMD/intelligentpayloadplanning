package GUI.InputPanel;

/**
 * Represents the updates.
 */
@FunctionalInterface
public interface UpdateConsumer
{
    /**
     * @param tubes
     *         # of tubes available in the hatch.
     * @param cells
     *         # of cells available int he cells.
     */
    void update(int tubes, int cells);
}
