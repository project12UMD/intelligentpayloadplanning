package GUI.InputPanel;

/**
 *  A Utility File for comparison with min and max values for restrictions.
 */
public final class Utils
{
    /**
     * @param min          min value
     * @param max          max value.
     * @param value        value
     * @param <T>          generic type to compare min and max values
     * @return value       value in the field
     */
    public static <T extends Comparable<? super T>> T restrict(T value, T min, T max)
    {
        if (min != null && value.compareTo(min) < 0)
        {
            return min;
        }
        if (max != null && value.compareTo(max) > 0)
        {
            return max;
        }

        return value;
    }
}
