package GUI.ListPanel;

import DataHandlers.PayloadList.DeploymentAction;
import DataHandlers.PayloadList.HatchAction;
import DataHandlers.PayloadList.PayloadAction;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import org.jetbrains.annotations.Nullable;

/**
 * This class lists all the actions that need to be taken to complete a mission.
 */

class CardPanel extends VBox
{
    /**
     * @param action TODO Add documentation.
     * @param time   TODO Add documentation.
     * @return TODO Add documentation.
     */
    public static CardPanel Construct(DeploymentAction action, Integer time)
    {
        if (action instanceof PayloadAction)
        {
            return new CardPanel((PayloadAction) action, time);
        }
        else
        {
            return new CardPanel((HatchAction) action, time);
        }
    }

    /**
     * This constructor gets the outputs from HatchAction then creates lables for them and displays them
     * on the card panel
     *
     * @param action This parameter indicates the action of the methods PayloadAction and HatchAction
     * @param time   TODO Add documentation.
     */
    private CardPanel(HatchAction action, @Nullable Integer time)
    {
        String actionText;
        if (action.getActionType() == HatchAction.TYPE.OPEN)
        {
            actionText = "Open";
        }
        else
        {
            actionText = "Close";
        }

        Label actionLabel = new Label(actionText + " Hatch ");
        actionLabel.setFont(Font.font(15));
        Label hatchLabel = new Label("Tube: " + (action.getTube() + 1));
        hatchLabel.setFont(Font.font(15));
        Label timeLabel = new Label("Start: " + action.getStartTime() + " secs");
        timeLabel.setFont(Font.font(15));
        this.getChildren().addAll(
                actionLabel,
                hatchLabel,
                timeLabel
        );
        if (time != null)
        {
            Label statusLabel = new Label();
            statusLabel.setFont(Font.font(15));
            switch (action.getStatus(time))
            {
                case WILL_CLOSE:
                    statusLabel.setText(" ");
                    break;
                case CLOSING:
                    statusLabel.setText("CLOSING");
                    break;
                case CLOSED:
                    statusLabel.setText("CLOSED");
                    break;
                case WILL_OPEN:
                    statusLabel.setText(" ");
                    break;
                case OPENING:
                    statusLabel.setText("OPENING");
                    break;
                case OPEN:
                    statusLabel.setText("OPEN");
                    break;
            }
            this.getChildren().add(statusLabel);
        }
        this.getStyleClass().add("card-info-box");
    }

    /**
     * This constructor gets the outputs from PayloadAction then creates lables for them and displays them
     * on the card panel. eg. Labels for location, tube, cell, etc.
     *
     * @param action This parameter indicates the action of the methods PayloadAction and HatchAction
     * @param time   TODO Add documentation.
     */
    private CardPanel(PayloadAction action, @Nullable Integer time)
    {
        Label actionLabel = new Label("Deploy Payload");
        actionLabel.setFont(Font.font(15));
        Label hatchCellLabel = new Label("Tube: "
                + (action.getTube() + 1)
                + ", Cell: "
                + (action.getCell() + 1));
        hatchCellLabel.setFont(Font.font(15));
        Label locationLabel = new Label("Location " + action.getTarget().getId());
        locationLabel.setFont(Font.font(15));
        Label timeLabel = new Label("Start: " + action.getStartTime() + " sec");
        timeLabel.setFont(Font.font(15));
        this.getChildren().addAll(
                actionLabel,
                hatchCellLabel,
                locationLabel,
                timeLabel
        );
        if (time != null)
        {
            Label statusLabel = new Label();
            statusLabel.setFont(Font.font(15));
            switch (action.getStatus(time))
            {
                case ARRIVED:
                    statusLabel.setText("ARRIVED");
                    break;
                case IN_TRANSIT:
                    statusLabel.setText("IN TRANSIT");
                    break;
                case LAUNCHING:
                    statusLabel.setText("LAUNCHING");
                    break;
                case WILL_LAUNCH:
                    statusLabel.setText(" ");
                    break;
            }
            this.getChildren().add(statusLabel);
        }
        this.getStyleClass().add("card-info-box");
    }

    /**
     * This constructor creates an arrow card.
     *
     * @param action TODO
     */
    CardPanel(HatchAction action)
    {
        this.getStyleClass().add("card-info-box");
    }
}
