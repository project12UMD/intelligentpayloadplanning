package GUI.ListPanel;

import GUI.GanttChart.GanttChart;
import DataHandlers.PayloadList.PayloadList;
import DataHandlers.PayloadList.*;

import GUI.UpdatablePanel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This Panel Displays the Actions needed to complete the mission
 */
public class ListPanel extends TabPane implements UpdatablePanel
{
    private HBox listHBox;

    Map<String, XYChart.Series<Number, String>> seriesList;
    private GanttChart<Number, String> chart;
    private NumberAxis xAxis;
    private CategoryAxis yAxis;

    /**
     * This constructor sets up the main ScrollPane
     */
    public ListPanel()
    {
        initComponents();
    }


    /**
     * These are the initial components for the Gantt Chart and the Card Panel
     */
    public void initComponents()
    {
        listHBox = new HBox();
        listHBox.getStyleClass().add("card-list-hbox");

        ScrollPane listScroller = new ScrollPane(listHBox);
        listScroller.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        listScroller.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);

        listHBox.minHeightProperty().bind(listScroller.heightProperty().subtract(20));
        listHBox.maxHeightProperty().bind(listScroller.heightProperty().subtract(20));

        xAxis = new NumberAxis();
        xAxis.setLabel("Time (sec)");
        xAxis.setTickLabelFill(Color.CHOCOLATE);
        xAxis.setMinorTickCount(5);
        xAxis.setTickUnit(1);
        xAxis.setTickMarkVisible(true);

        yAxis = new CategoryAxis();
        yAxis.setLabel("");
        yAxis.setTickLabelFill(Color.CHOCOLATE);
        yAxis.setTickLabelGap(10);
        yAxis.tickLabelFontProperty().set(Font.font(15));

        chart = new GanttChart<>(xAxis, yAxis);
        chart.setTitle("Deployment Actions");
        chart.setLegendVisible(false);
        chart.setBlockHeight(20);
        chart.setMinHeight(200);

        ScrollPane chartScroller = new ScrollPane(chart);
        chartScroller.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        chartScroller.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);

        Tab listTab = new Tab("List View");
        listTab.setContent(listScroller);
        listTab.setClosable(false);

        Tab chartTab = new Tab("Chart View");
        chartTab.setContent(chartScroller);
        chartTab.setClosable(false);

        this.getTabs().addAll(listTab, chartTab);
        this.setMinHeight(200);
        this.setMaxHeight(300);
        this.getStyleClass().add("root");
        this.getStylesheets().add("global.css");
    }

    /**
     * This is the update method with actions from the given PayloadList
     *
     * @param list the PayloadList from which to generate the HatchAction and DeploymentAction cards
     */
    public void update(PayloadList list)
    {
        if(seriesList != null)chart.getData().removeAll(seriesList.values());
        ObservableList<String> tubeID = FXCollections.observableArrayList();
        yAxis.setCategories(tubeID);
        for (int i = 1; i <= list.getConstraints().getTubeAmount(); i++)
        {
            tubeID.add("Tube " + i);
            for (int k = 1; k <= list.getConstraints().getCellAmount(); k++)
            {
                tubeID.add("Tube " + i + ", " + "Cell " + k);
            }
        }

        int numLabels = tubeID.size();
        double blockHeight = chart.getBlockHeight();
        chart.setMinHeight(numLabels * (blockHeight + yAxis.getTickLabelGap()) + 100);

        listHBox.getChildren().clear();

        for (DeploymentAction action : list.getOrderList())
        {
            listHBox.getChildren().add(CardPanel.Construct(action, null));
        }
       // listHBox.getChildren().remove(listHBox.getChildren().size() - 1);

        seriesList = new HashMap<>();
        for (Sequence sequence : list.getSequenceList())
        {
            for (DeploymentAction action : sequence.getUnmodifiableList())
            {
                int tube = action.getTube() + 1;
                int startTime = action.getStartTime();
                int actionTime = action.getActionTime();

                if (action instanceof HatchAction)
                {
                    HatchAction hatchAction = (HatchAction) action;
                    String actionID = "Tube " + tube;
                    if (hatchAction.getActionType() == HatchAction.TYPE.OPEN)
                    {
                        insertData(startTime, actionID, actionTime, "event-hatch-open");
                    }
                    else
                    {
                        insertData(startTime, actionID, actionTime, "event-hatch-close");
                    }
                }
                else
                {
                    PayloadAction payloadAction = (PayloadAction) action;
                    int cell = payloadAction.getCell() + 1;
                    int transitStart = startTime + actionTime;
                    int transitTime = payloadAction.getTarget().getTravelTime();
                    String actionID = "Tube " + tube + ", " + "Cell " + cell;

                    insertData(startTime, actionID, actionTime, "event-payload-launch");
                    insertData(transitStart, actionID, transitTime, "event-payload-arrival");
                }
            }
        }
    }

    /**
     * @param startTime
     *      Start time of the Actions in the sequence list
     * @param actionID
     *      Id of the action whether it is tube or cell
     * @param actionTime
     *      Time it takes for the action to complete
     * @param style
     *      Color of the block for each action
     */
    private void insertData(int startTime, String actionID, int actionTime, String style)
    {
        XYChart.Series<Number, String> series;
        if (seriesList.containsKey(actionID))
        {
            series = seriesList.get(actionID);
        }
        else
        {
            series = new XYChart.Series<>();
            seriesList.put(actionID, series);
            chart.getData().add(series);
        }

        series.getData().add(
                new XYChart.Data<>(
                        startTime,
                        actionID,
                        new GanttChart.ExtraData(actionTime, style)
                )
        );
    }


    @Override
    public void update(PayloadList list, int time) {
        //TODO Do this.
        if(!chart.getData().isEmpty())chart.getData().removeAll(seriesList.values());

        listHBox.getChildren().clear();

        for (DeploymentAction action : list.getOrderList())
        {
            if(action.getStartTime() > time) continue;
            listHBox.getChildren().add(CardPanel.Construct(action, time));
            if(action instanceof HatchAction){
                switch(((HatchAction)action).getStatus(time)){
                    case OPEN:
                    case CLOSED:
                        listHBox.getChildren().get(listHBox.getChildren().size()-1).setDisable(true);
                        break;
                    default: //do nothing
                }


            }
            if(action instanceof PayloadAction){
                if(((PayloadAction) action).getStatus(time) == PayloadAction.STATUS.ARRIVED) listHBox.getChildren().get(listHBox.getChildren().size()-1).setDisable(true);
            }
        }

        ObservableList<String> tubeID = FXCollections.observableArrayList();
        yAxis.setCategories(tubeID);
        for (int i = 1; i <= list.getConstraints().getTubeAmount(); i++)
        {
            tubeID.add("Tube " + i);
            for (int k = 1; k <= list.getConstraints().getCellAmount(); k++)
            {
                tubeID.add("Tube " + i + ", " + "Cell " + k);
            }
        }

        int numLabels = tubeID.size();
        double blockHeight = chart.getBlockHeight();
        chart.setMinHeight(numLabels * (blockHeight +
                yAxis.getTickLabelGap()) + 100);

        seriesList = new HashMap<>();

        for (Sequence sequence : list.getSequenceList())
        {
            for (DeploymentAction action : sequence.getUnmodifiableList())
            {
                if(action.getStartTime() > time) continue;
                int tube = action.getTube() + 1;
                int startTime = action.getStartTime();
                int actionTime = action.getActionTime();

                if (action instanceof HatchAction)
                {
                    HatchAction hatchAction = (HatchAction) action;
                    String actionID = "Tube " + tube;
                    switch(hatchAction.getStatus(time)){
                        case OPENING:
                        case OPEN:
                            insertData(startTime, actionID,
                                    actionTime, "event-hatch-open");
                            break;
                        case WILL_CLOSE:
                        case CLOSING:
                        case CLOSED:
                            insertData(startTime, actionID,
                                    actionTime, "event-hatch-close");
                            break;
                        default:
                            //the other status types
                    }
                } else
                {
                    PayloadAction payloadAction = (PayloadAction) action;
                    int cell = payloadAction.getCell() + 1;
                    int transitStart = startTime + actionTime;
                    int transitTime = payloadAction.getTarget().getTravelTime();
                    String actionID = "Tube " + tube + ", " + "Cell " + cell;

                    //Where is this method located?
                    insertData(startTime, actionID, actionTime,
                            "event-payload-launch");
                    insertData(transitStart, actionID, transitTime,
                            "event-payload-arrival");
                    switch(payloadAction.getStatus(time)){
                        case LAUNCHING:
                        case IN_TRANSIT:
                            insertData(startTime, actionID,
                                    actionTime, "event-payload-launch");
                        case ARRIVED:
                            insertData(transitStart, actionID,
                                    transitTime, "event-payload-arrival");
                        default:
                            //someting else
                    }
                }
            }
        }
    }


    @Override
    public void update(int tubes, int cells) {
    }


}
