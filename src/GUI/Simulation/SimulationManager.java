package GUI.Simulation;

import DataHandlers.PayloadList.PayloadList;
import GUI.UpdatablePanel;
import javafx.scene.control.ProgressBar;

import java.util.List;
import java.util.function.Supplier;

/**
 * Manages the simulation. Ensures that all the panels are updated at the correct times.
 */
public class SimulationManager
{
    private TimingTask timing;
    private int time;
    private Runnable completionTask;
    private Supplier<ProgressBar> progressBarSupplier;
    private List<UpdatablePanel> panels;

    /**
     * Sets the supplier for progress bars that will be updated by the simulation's progress.
     *
     * @param supplier
     *         Supplies progress bars that will be updated with the simulation's progress.
     */
    public void setProgressBarSupplier(Supplier<ProgressBar> supplier)
    {
        progressBarSupplier = supplier;
    }

    /**
     * Sets the panels for this simulation to output to.
     *
     * @param panels
     *         The list of panels.
     */
    public void setOutputPanels(List<UpdatablePanel> panels)
    {
        this.panels = panels;
    }

    /**
     * Starts the simulation at the given time. At each second the simulation will call the update methods in each of
     * the given panels.
     *
     * @param startTime
     *         The time that the simulation will start.
     * @param endTime
     *         The time that the simulation will end.
     * @param currentList
     *         The PayloadList that will be used to run the simulation.
     */
    public void start(int startTime, int endTime, PayloadList currentList)
    {
        time = startTime;
        timing = new TimingTask(startTime, endTime);
        if(progressBarSupplier != null)
        {
            progressBarSupplier.get()
                               .progressProperty()
                               .bind(timing.progressProperty());
        }
        if(panels != null)
        {
            timing.workDoneProperty()
                  .addListener((obs, old, val) ->
                               {
                                   for(UpdatablePanel i : panels)
                                   {
                                       time = val.intValue();
                                       i.update(currentList, time);
                                   }
                               });
        }
        timing.setOnSucceeded((a) ->
                              {
                                  if(completionTask != null)
                                  {
                                      completionTask.run();
                                  }
                              });
        new Thread(timing).start();
    }

    /**
     * Stops the simulation and returns the time that the simulation stopped at.
     *
     * @return The time the simulation stopped.
     */
    public int stop()
    {
        timing.cancel();
        return time;
    }

    /**
     * Sets a runnable method that will be run on completion of the simulation.
     *
     * @param completionTask
     *         The runnable to run on the completion of the simulation.
     */
    public void onCompletion(Runnable completionTask)
    {
        this.completionTask = completionTask;
    }
}
