package GUI.Simulation;

import javafx.concurrent.Task;

import java.util.concurrent.TimeUnit;

/**
 * A task used to update all the panels in the application every second.
 */
class TimingTask extends Task<Void>
{
    private int time;
    private int maxTime;

    /**
     * Creates a new TimingTask object.
     *
     * @param startTime
     *         The start time for the simulation.
     * @param maxTime
     *         The end time for the simulation.
     */
    TimingTask(int startTime, int maxTime)
    {
        time = startTime;
        this.maxTime = maxTime;
        updateProgress(time, maxTime);
    }

    @Override
    protected Void call() throws Exception
    {
        while(time < maxTime)
        {
            if(isCancelled())
            {
                time = maxTime;
            }
            else
            {
                TimeUnit.SECONDS.sleep(1);
                time++;
                updateProgress(time, maxTime);
            }
        }
        return null;
    }
}