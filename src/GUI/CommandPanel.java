package GUI;

import DataHandlers.Constraint.Constraint;
import DataHandlers.PayloadList.PayloadList;
import GUI.DiagramPanel.DiagramPanel;
import GUI.InputPanel.InputPanel;
import GUI.Simulation.SimulationManager;
import Optimizer.OptimizationFunction;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A panel that includes all the command buttons that brings the application from one state to another.
 */
class CommandPanel extends BorderPane
{
    private List<UpdatablePanel> panels;
    private List<Button> buttonList = new ArrayList<>();
    private InputPanel input;
    private OptimizationFunction optimizer;
    private SimulationManager simulation;
    private PayloadList currentList;
    private int currentTime = 0;

    private Button optimizeButton;
    private Button resetButton;
    private Button stopButton;
    private Button runButton;
    private Button pauseButton;
    private GridPane buttonGrid;

    private ApplicationState applicationState;

    /**
     * Creates a new CommandPanel
     *
     * @param input
     *         The input panel. Changes this panel state depending on the application's state.
     * @param optimizer
     *         The optimizer that is used to calculate the PayloadList.
     */
    CommandPanel(InputPanel input, OptimizationFunction optimizer)
    {
        this.input = input;
        this.optimizer = optimizer;

        optimizeButton = new Button("Optimize");
        optimizeButton.setPrefWidth(1000);
        optimizeButton.getStyleClass()
                      .add("dark-optimize-button");

        resetButton = new Button("Reset");
        resetButton.setPrefWidth(1000);
        resetButton.getStyleClass()
                   .add("dark-reset-button");
        resetButton.setDisable(true);

        stopButton = new Button("Stop");
        stopButton.setPrefWidth(1000);
        stopButton.getStyleClass()
                  .add("dark-stop-button");
        stopButton.setDisable(true);

        runButton = new Button("Run");
        runButton.setPrefWidth(1000);
        runButton.getStyleClass()
                 .add("dark-run-button");
        runButton.setDisable(true);

        pauseButton = new Button("Pause");
        pauseButton.setPrefWidth(1000);
        pauseButton.getStyleClass()
                   .add("dark-pause-button");
        pauseButton.setDisable(true);

        buttonList.addAll(Arrays.asList(runButton,pauseButton,stopButton,optimizeButton,resetButton));

        buttonGrid = new GridPane();
        buttonGrid.add(optimizeButton, 0, 0);
        this.setCenter(buttonGrid);
        this.getStyleClass()
            .add("root");
        this.getStylesheets()
            .add("global.css");

        optimizeButton.setOnAction((e) -> optimizeTransition());
        resetButton.setOnAction((e) -> resetTransition());
        runButton.setOnAction((e) -> runTransition());
        stopButton.setOnAction((e) -> stopTransition());
        pauseButton.setOnAction((e) -> pauseTransition());

        applicationState = ApplicationState.INPUT;
        simulation = new SimulationManager();
        simulation.onCompletion(this::pauseTransition);
    }

    /**
     * Sets all the panels that will be updated on state changes.
     *
     * @param panels
     *         The output display panels.
     */
    void setOutputPanels(UpdatablePanel... panels)
    {
        this.panels = Arrays.asList(panels);
        Constraint constraints = input.getConstraints();
        simulation.setOutputPanels(Arrays.asList(panels));
        for(UpdatablePanel panel : panels)
        {
            panel.update(constraints.getTubeAmount(), constraints.getCellAmount());
            if(panel instanceof InfoPanel)
            {
                simulation.setProgressBarSupplier(((InfoPanel) panel)::getTimeBar);
            }
        }
    }

    /**
     * Transition from 'Input' state to 'Output' state.
     */
    private void optimizeTransition()
    {
        for(Button button:buttonList){
            button.setDisable(true);
        }
        if(applicationState != ApplicationState.INPUT)
        {
            throw new IllegalStateException("Application in wrong state. Current State: " + applicationState);
        }

        try{
            currentList = optimizer.apply(input.getConstraints());
        }catch(IllegalStateException minListNotFound){
            int totalAvailableCells = 0;
            for(int currentTube = 0 ; currentTube < input.getConstraints().getTubeAmount(); currentTube++) totalAvailableCells += input.getConstraints().getAvailableCells(currentTube);
            String response =  totalAvailableCells < input.getConstraints().getNeededPayloads() ?
                    "The amount of available cells is less than the needed payloads" : "Please make more than 1 payload";
            Alert alert = new Alert(Alert.AlertType.ERROR, response);
            alert.setHeaderText("Input Error");
            alert.showAndWait();
            for(Button button:buttonList){
                button.setDisable(false);
            }
            return;
        }
        buttonGrid.getChildren()
                  .clear();
        buttonGrid.add(resetButton, 0, 0);
        buttonGrid.add(runButton, 1, 0);


        //currentList = optimizer.apply(input.getConstraints());
        for(UpdatablePanel i : panels)
        {
            i.update(currentList);
        }
        input.setDisable(true);
        applicationState = ApplicationState.OUTPUT;
        resetButton.setDisable(false);
        runButton.setDisable(false);
    }

    /**
     * Transition from 'Output' state to 'Input' state.
     */
    private void resetTransition()
    {
        for(Button button:buttonList){
            button.setDisable(true);
        }
        if(applicationState != ApplicationState.OUTPUT)
        {
            throw new IllegalStateException("Application in wrong state. Current State: " + applicationState);
        }

        buttonGrid.getChildren()
                  .clear();
        buttonGrid.add(optimizeButton, 0, 0);
        input.setDisable(false);
        input.callUpdate();

        for(UpdatablePanel i : panels)
        {
            i.update(currentList.getConstraints()
                                .getTubeAmount(), currentList.getConstraints()
                                                             .getCellAmount());
            if(i instanceof DiagramPanel){
                ((DiagramPanel) i).editable(true);
            }
        }
        applicationState = ApplicationState.INPUT;
        optimizeButton.setDisable(false);
    }

    /**
     * Transition from 'SimulationManager Paused' state or 'Output' state to 'SimulationManager Running' state.
     */
    private void runTransition()
    {
        for(Button button:buttonList){
            button.setDisable(true);
        }
        if(!(applicationState == ApplicationState.SIMULATION_PAUSED
                || applicationState == ApplicationState.OUTPUT))
        {
            throw new IllegalStateException("Application in wrong state. Current State: " + applicationState);
        }

        if(currentTime == 0)
        {
            for(UpdatablePanel i : panels)
            {
                i.update(currentList, 0);
            }
        }
        buttonGrid.getChildren()
                  .clear();
        buttonGrid.add(pauseButton, 0, 0);
        if(currentTime >= currentList.getArrivalTime())
        {
            currentTime = 0;
        }
        simulation.start(currentTime, currentList.getArrivalTime(), currentList);
        applicationState = ApplicationState.SIMULATION_RUNNING;
        pauseButton.setDisable(false);
    }

    /**
     * Transition from 'SimulationManager Paused' state to 'Output' state.
     */
    private void stopTransition()
    {
        for(Button button:buttonList){
            button.setDisable(true);
        }
        if(applicationState != ApplicationState.SIMULATION_PAUSED)
        {
            throw new IllegalStateException("Application in wrong state. Current State:" + applicationState);
        }

        currentTime = 0;
        buttonGrid.getChildren()
                  .clear();
        buttonGrid.add(resetButton, 0, 0);
        buttonGrid.add(runButton, 1, 0);

        currentTime = 0;
        for(UpdatablePanel i : panels)
        {
            i.update(currentList);
        }
        applicationState = ApplicationState.OUTPUT;
        resetButton.setDisable(false);
        runButton.setDisable(false);
    }

    /**
     * Transition from 'SimulationManager Running' state to 'SimulationManager Paused' state.
     */
    private void pauseTransition()
    {
        for(Button button:buttonList){
            button.setDisable(true);
        }
        if(applicationState != ApplicationState.SIMULATION_RUNNING)
        {
            throw new IllegalStateException("Application in wrong state. Current State: " + applicationState);
        }

        buttonGrid.getChildren()
                  .clear();
        buttonGrid.add(stopButton, 0, 0);
        buttonGrid.add(runButton, 1, 0);

        for(UpdatablePanel i : panels)
        {
            if(i instanceof DiagramPanel) ((DiagramPanel) i).editable(true);
        }

        currentTime = simulation.stop();
        applicationState = ApplicationState.SIMULATION_PAUSED;
        stopButton.setDisable(false);
        runButton.setDisable(false);
    }

    /**
     * Represents the current state of the application. Used for error checking purposes to ensure illegal transitions
     * of application state are not made.
     */
    private enum ApplicationState
    {
        INPUT, OUTPUT, SIMULATION_RUNNING, SIMULATION_PAUSED
    }

}
