package GUI.DiagramPanel;

import DataHandlers.Constraint.CellStatuses;
import DataHandlers.PayloadList.*;
import GUI.UpdatablePanel;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

/**
 * This panel displays a list of actions that must be taken to complete a mission.
 */
public class DiagramPanel extends ScrollPane implements UpdatablePanel {
    public HBox topRow, bottomRow;
    public ArrayList<Tube> tubes = new ArrayList<>();
    private AnchorPane anchor;
    private int tubesVisible = 0;
    private Runnable editHandler;
    private boolean userEditMode;
    private PayloadList currentList;
    private int currentTime;


    /**
     * Constructor to create the diagram panel
     */
    public DiagramPanel() {
        this.getStyleClass()
                .add("root");
        this.getStylesheets()
                .add("global.css");
        topRow = new HBox();
        bottomRow = new HBox();
        topRow.setPrefSize(800,150);
        bottomRow.setPrefSize(800, 150);
        VBox holder = new VBox(topRow, bottomRow);
        holder.setPrefSize(800, 300);
        anchor = new AnchorPane(holder);
        anchor.setPrefSize(800, 300);
        setPrefSize(1100.0, 300.0);
        setContent(anchor);
        setHbarPolicy(ScrollBarPolicy.ALWAYS);
        heightProperty().addListener((a)-> {
            if(!userEditMode){
                return;
            }
            for(Tube tube : tubes){
                tube.redraw(getHeight()/2.1);
             }
            if(currentList != null){
                if(currentTime != 0) {
                    update(currentList, currentTime);
                    return;
                }
                update(currentList);
            }

            });

        setOnMouseReleased((e)->{
            if(editHandler != null) editHandler.run();
        });


    }

    /**
     * constructor for the window popup
     *
     * @param windowWidth how big the window will be
     */
    public DiagramPanel(double windowWidth) {
        this.getStyleClass()
                .add("root");
        this.getStylesheets()
                .add("global.css");
        topRow = new HBox();
        bottomRow = new HBox();
        topRow.setPrefSize(800,150);
        bottomRow.setPrefSize(800, 150);
        VBox holder = new VBox(topRow, bottomRow);
        holder.setPrefSize(800, 300);
        anchor = new AnchorPane(holder);
        anchor.setPrefSize(800, 300);
        setPrefSize(windowWidth, 300.0);
        setContent(anchor);
        setHbarPolicy(ScrollBarPolicy.ALWAYS);
        heightProperty().addListener((a)-> {
            if(!userEditMode){
                return;
            }
            for(Tube tube : tubes){
                tube.redraw(getHeight()/2.1);
            }
            if(currentList != null){
                if(currentTime != 0) {
                    update(currentList, currentTime);
                    return;
                }
                update(currentList);
            }

        });

        setOnMouseReleased((e)->{
            if(editHandler != null) editHandler.run();
        });


    }
    /**
     * Sets what should happen after this diagram is edited by the user..
     *
     * @param handler The action to be taken on a change of the diagram.
     */
    public void setOnEdit(Runnable handler) {
        editHandler = handler;
    }
    //TODO will be updated with many runnables, check by sunday
    /**
     * Updates the diagram panel using data from a payloadlist.
     *
     * @param list the list that comes from the optimizer
     * @throws IllegalStateException cannot make more than 20 tubes
     */
    public void update(PayloadList list) {
        editable(false);
        currentList = list;
        if(editHandler != null) editHandler.run();
        //remove previous tubes and cells made if any
        if (tubes.size() != 0) {
            while (tubesVisible != 0) {
                if (tubesVisible%2 == 0){
                    bottomRow.getChildren().remove(bottomRow.getChildren().size()-1);
                }
                else{
                    topRow.getChildren().remove(topRow.getChildren().size()-1);
                    if(tubesVisible > 10 && getWidth() > 800) {
                        anchor.setPrefWidth(anchor.getPrefWidth() - 150);
                    }
                }
                tubesVisible--;
            }
        }


        //redraw tubes & cells
        for (int i = 0; i < list.getConstraints().getTubeAmount(); i++) {
            if (tubesVisible >= 20) {
                throw new IllegalStateException("Max tubes reached.");
            } else if (tubes.size() == tubesVisible && tubesVisible < 20) {  //need to make new tube
                Tube newTube = new Tube(getHeight()/2);
                newTube.drawTube(list.getConstraints()
                        .getCellAmount());
                tubes.add(newTube);
                displayTube(newTube);
            } else {
                Tube previousTube = tubes.get(tubesVisible);
                previousTube.drawTube(list.getConstraints()
                        .getCellAmount());

                for (int cell = 0; cell < previousTube.getCellCount(); cell++) {
                    if (list.getCellStatuses()[i][cell] == CellStatus.LAUNCHED)
                        previousTube.finishCell(cell);
                }

                displayTube(previousTube);
            }
        }

        int simulShots = list.getConstraints().getDeploymentLimit();
        int order = 1;
        int firedCell = 0;
        //Set cell fired
        for(int tube = 0; tube < tubesVisible; tube++)
        {
            for(int cell = 0; cell < tubes.get(tube).getCellCount(); cell++)
            {
                if(list.getCellStatuses()[tube][cell] == CellStatus.LAUNCHED)
                {
                    //redraw tube to show it has been shot
                    tubes.get(tube).finishCell(cell, order);
                    firedCell++;
                    if(firedCell%simulShots == 0) order++;
                }

            }

        }


    }

    /**
     * Updates the panel with the given payload list and has it represent the given time.
     *
     * @param list The data the panel should be updated with.
     * @param time The time in seconds that have passed.
     */
    public void update(PayloadList list, int time) {
        editable(false);
        currentTime = time;

        if(time == 0){
            for(int i = 0; i < list.getConstraints().getTubeAmount() ; i++){
                tubes.get(i).closeHatch();
            }
        }

        for (DeploymentAction action : list.getOrderList()) {
            if(action instanceof HatchAction)
            {
                HatchAction hatch = ((HatchAction) action);
                switch(hatch.getStatus(time)){
                    case CLOSED:
                    case CLOSING:
                        tubes.get(hatch.getTube()).closeHatch();
                        break;
                    case OPEN:
                    case OPENING:
                        tubes.get(hatch.getTube()).openHatch();
                        break;
                    default:
                        //System.out.println("tube will close");
                }
            }
            else{
                PayloadAction payload = ((PayloadAction) action);
                switch (payload.getStatus(time)){
                    case WILL_LAUNCH:
                        tubes.get(payload.getTube()).prepareCell(payload.getCell());
                        break;
                    case IN_TRANSIT:
                    case LAUNCHING:
                        tubes.get(payload.getTube()).fireCell(payload.getCell());
                        break;
                    case ARRIVED:
                        tubes.get(payload.getTube()).finishCell(payload.getCell());
                        break;
                    default:
                        System.out.println("error");
                }
            }
        }

    }

    /**
     * Updates the diagram panel with the amount of cells and tubes for the User.
     * initial user edit mode
     *
     * @param tubesWanted The amount of tubes.
     * @param cells       The amount of cells
     */

    public void update(int tubesWanted, int cells) {
        editable(true);
        currentList = null;
        currentTime = 0;

        //remove previous tubes and cells made if any
        if (tubes.size() != 0) {
            while (tubesVisible != 0) {
                if (tubesVisible%2 == 0){
                    bottomRow.getChildren().remove(bottomRow.getChildren().size()-1);
                }
                else{
                    topRow.getChildren().remove(topRow.getChildren().size()-1);
                    if(tubesVisible > 10 || getWidth() > 800) {
                        anchor.setPrefWidth(anchor.getPrefWidth() - 150);
                    }
                }
                tubesVisible--;
            }
        }

        //redraw tubes & cells
        for (int i = 0; i < tubesWanted; i++) {
            if (tubesVisible >= 20) {
                throw new IllegalStateException("Max tubes reached.");
            } else if (tubes.size() == tubesVisible && tubesVisible < 20) {  //need to make new tube
                Tube newTube = new Tube(getHeight()/2.1);
                newTube.drawTube(cells);
                tubes.add(newTube);
                displayTube(newTube);
            } else {
                Tube previousTube = tubes.get(tubesVisible);
                previousTube.drawTube(cells);
                displayTube(previousTube);
            }
        }

        if(editHandler != null) editHandler.run();
    }

    /**
     * Fetches the current settings of all the cells.
     *
     * @return The status of all the cells.
     */
    public CellStatuses fetchCellStatuses() {

        CellStatuses cellStats = new CellStatuses(tubesVisible, tubes.get(0)
                .getCellCount());
        for (int seenTube = 0 ; seenTube < tubesVisible; seenTube++) {
            for (int cell = 0; cell < tubes.get(seenTube).getCellCount(); cell++)
                cellStats.setStatus(tubes.get(seenTube).getTubeNumber() - 1, cell, !tubes.get(seenTube).getFaultStatus(cell));
        }
        return cellStats;
    }

    /**
     * Sets if the diagram panel will be editable by the user.
     *
     * @param setting True if the user can edit the diagram, false otherwise.
     */
    public void editable(boolean setting) {
        userEditMode = setting;
        for(Tube tube : tubes){
            tube.canEdit = setting;
        }
    }

    /**
     * displays a tube onto the top or bottom Hbox that is inside their own scroll pane
     *
     * @param tube the tube that will be displayed
     */
    private void displayTube(Tube tube) {
        if (topRow.getChildren()
                .size() == bottomRow.getChildren()
                .size()) {
            if (tubesVisible >= 10 && tubesVisible % 2 == 0) {
                anchor.setPrefWidth(anchor.getPrefWidth() + 150);
            }
            topRow.getChildren()
                    .add(tube);
            tubesVisible++;
        } else {
            bottomRow.getChildren()
                    .add(tube);
            tubesVisible++;
        }

    }


}
