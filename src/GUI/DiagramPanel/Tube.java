package GUI.DiagramPanel;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontSmoothingType;

import java.util.ArrayList;

/**
 * Characteristics of a Tube found here, extends Canvas class
 * <p>
 * cell color representation
 * darkred = fired and finished
 * orangered = fired and in transit
 * yellow = waiting for launched
 * green = available
 * grey = faulted
 * darkgray = unavailable
 */
public class Tube extends Canvas
{

    private static int tubesMade = 1;
    private double tubePointXY;
    private double tubeSize;
    public double centerPoint;
    private double cellSize;
    private int cells = 0;
    private final int tubeNumber;
    private ArrayList<Double> cellLocation = new ArrayList<>();
    private ArrayList<Boolean> isFaulted = new ArrayList<>();
    private ArrayList<Boolean> isFired = new ArrayList<>();
    private ArrayList<Boolean> isWaiting = new ArrayList<>();
    private ArrayList<Boolean> isFinished = new ArrayList<>();
    private boolean[] userFault = new boolean[10];
    public boolean canEdit = true;


    /**
     * constructors that creates tube and sets canvas width
     *
     * @param size the size of the canvas to display the tube
     *             correlates to half the size of diagram panel
     */
    public Tube(double size)
    {
        this.setWidth(size);
        this.setHeight(size);
        this.tubePointXY = getWidth() / 6;
        this.tubeSize = getWidth() / 1.5;
        this.cellSize = getWidth()/15;
        this.centerPoint = (getWidth() / 2) - cellSize/2;
        this.tubeNumber = tubesMade;

        GraphicsContext drawLabel = this.getGraphicsContext2D();

        //draw tube hatch label
        drawLabel.setFill(Color.WHITE);
        drawLabel.setLineWidth(1d);
        drawLabel.setFont(Font.font("Verdana", cellSize));
        drawLabel.setFontSmoothingType(FontSmoothingType.LCD);
        drawLabel.fillText("Hatch #" + tubeNumber, 35d, getWidth() - 10);
        this.setOnMousePressed((e) -> clickedOn(e.getX(), e.getY()));
        tubesMade++;
    }


    /**
     * gets cell waiting from the tube
     *
     * @param cell
     *         the specific cell to see status
     *
     * @return the status of the cell false = not waiting true = waiting
     */
    public boolean getWaitingStatus(int cell)
    {
        return isWaiting.get(cell);
    }

    /**
     * the tube drawing function, has old lines of code to help keep the tubes lined u
     *
     * @param cellsToMake
     *         the number of cells needed to make
     */
    public void drawTube(int cellsToMake)
    {

        cells = cellsToMake;

        GraphicsContext drawingTube = this.getGraphicsContext2D();

        //set fills and stroke
        drawingTube.setFill(Color.BLUE);
        drawingTube.setStroke(Color.BLACK);
        drawingTube.setLineWidth(2.5d);

        //draw TUbe
        drawingTube.fillOval(tubePointXY, tubePointXY, tubeSize, tubeSize);
        drawingTube.strokeOval(tubePointXY, tubePointXY, tubeSize, tubeSize);

        //remove last cell positions & states
        cellLocation.clear();
        isFaulted.clear();
        isFired.clear();
        isFinished.clear();
        isWaiting.clear();

        drawingTube.setLineWidth(1d);
        // make sure all have same amount of cells
        if(cellsToMake == 1)
        {
            if(userFault[0])
            {
                drawingTube.setFill(Color.GRAY);
                isFaulted.add(true);
            }
            else
            {
                drawingTube.setFill(Color.GREEN);
                isFaulted.add(false);
            }
            drawingTube.fillOval(centerPoint, centerPoint, cellSize, cellSize);
            cellLocation.add(centerPoint);
            cellLocation.add(centerPoint);

            drawingTube.setFill(Color.YELLOW);
            drawingTube.fillText("1", centerPoint, centerPoint - 2);

            isFired.add(false);
            isWaiting.add(false);
            isFinished.add(false);
        }
        else {
            int cellLabel = 1;
            for (double i = 0.0; i < 360; i += (360.0 / (cellsToMake))) {

                if (userFault[cellLabel - 1]) {
                    drawingTube.setFill(Color.GRAY);
                    isFaulted.add(true);
                } else {
                    drawingTube.setFill(Color.GREEN);
                    isFaulted.add(false);
                }
                double pointX = centerPoint + (Math.cos(Math.toRadians(i + 270)) * centerPoint / 2);
                double pointY = centerPoint + (Math.sin(Math.toRadians(i + 270)) * centerPoint / 2);
                drawingTube.fillOval(pointX, pointY, cellSize, cellSize);

                drawingTube.setFill(Color.YELLOW);
                drawingTube.fillText("" + cellLabel, pointX , pointY - 2);
                ++cellLabel;

                cellLocation.add(pointX);
                cellLocation.add(pointY);
                isFired.add(false);
                isWaiting.add(false);
                isFinished.add(false);
            }
        }
    }

    /**
     * returns the amount of cells the tube has
     *
     * @return the amount of cells a tube currently has
     */
    public int getCellCount()
    {
        return cells;
    }

    /**
     * a representation to show that a cell is fired
     *
     * @param cellNo
     *         the cell number inside the tube
     *
     */
    public void fireCell(int cellNo)
    {
        // get context of tube
        GraphicsContext drawingTube = this.getGraphicsContext2D();

        //done waiting
        isWaiting.set(cellNo, false);

        //draw the cell to show it has been fired
        drawingTube.setFill(Color.ORANGERED);
        drawingTube.fillOval(cellLocation.get(cellNo * 2), cellLocation.get(cellNo * 2 + 1), cellSize, cellSize);

        isFired.set(cellNo, true);

    }

    /**
     * a representation to show that a cell is waiting for launch
     *
     * @param cellNo
     *         the cell number inside the tube
     */
    public void prepareCell(int cellNo)
    {
        // get context of tube
        GraphicsContext drawingTube = this.getGraphicsContext2D();

        //cannot make a fired cell faulted
        if(this.isFaulted.get(cellNo)) { return; }

        //draw the cell to show it has been set to waiting
        drawingTube.setFill(Color.YELLOW);
        drawingTube.fillOval(cellLocation.get(cellNo * 2), cellLocation.get(cellNo * 2 + 1), cellSize, cellSize);

        isWaiting.set(cellNo, true);
    }

    /**
     * a representation to show that a cell has reached destination.
     *
     * @param cellNo
     *         the cell number inside the tube
     *
     * a newly drawn tube showing the finished cell
     */
    public void finishCell(int cellNo)
    {
        // get context of tube
        GraphicsContext drawingTube = this.getGraphicsContext2D();

        //draw the cell to show it has been set to waiting
        drawingTube.setFill(Color.DARKRED);
        drawingTube.fillOval(cellLocation.get(cellNo * 2), cellLocation.get(cellNo * 2 + 1), cellSize, cellSize);


        isFinished.set(cellNo, true);


    }

    /**
     * a representation to show that a cell has reached destination with the order of dhot.
     *
     * @param cellNo
     *         the cell number inside the tube
     *
     * @param shotOrder the order of the cell being shot
     *
     * a newly drawn tube showing the finished cell
     */
    public void finishCell(int cellNo, int shotOrder)
    {
        // get context of tube
        GraphicsContext drawingTube = this.getGraphicsContext2D();

        //draw the cell to show it has been set to waiting
        drawingTube.setFill(Color.DARKRED);
        drawingTube.fillOval(cellLocation.get(cellNo * 2), cellLocation.get(cellNo * 2 + 1), cellSize, cellSize);

        drawingTube.setFill(Color.WHITE);
        drawingTube.setFont(Font.font(cellSize));
        drawingTube.fillText("" + shotOrder, cellLocation.get(cellNo * 2) + cellSize/5 , cellLocation.get(cellNo * 2 + 1) + cellSize - 2.5);

        isFinished.set(cellNo, true);


    }
    /**
     * gets cell tube number from the tube
     *
     * @return the number of the tube
     */
    public int getTubeNumber()
    {
        return tubeNumber;
    }

    /**
     * gets cell fault status from the tube
     *
     * @param cell
     *         the specific cell to see status
     *
     * @return the status of the cell false = not Faulted true = faulted
     */
    public boolean getFaultStatus(int cell)
    {
        return isFaulted.get(cell);
    }

    /**
     * handles closing tube
     */
    public void closeHatch()
    {
        GraphicsContext drawHatch = this.getGraphicsContext2D();
        drawHatch.setFill(Color.rgb(0,0,0,0.4));
        drawHatch.fillOval(tubePointXY, tubePointXY, tubeSize, tubeSize);
    }


    /**
     * represents the tube being open
     */

    void openHatch()
    {
        GraphicsContext openDraw = this.getGraphicsContext2D();

        //set fills and stroke
        openDraw.setFill(Color.BLUE);
        openDraw.setStroke(Color.BLACK);
        openDraw.setLineWidth(2.5d);

        //draw TUbe
        openDraw.fillOval(tubePointXY, tubePointXY, tubeSize, tubeSize);
        openDraw.strokeOval(tubePointXY, tubePointXY, tubeSize, tubeSize);

        if(this.getCellCount() == 1)
        {
            if(userFault[0])
            {
                openDraw.setFill(Color.DARKGRAY);
                isFaulted.add(true);
            }
            else
            {
                openDraw.setFill(Color.GREEN);
                isFaulted.add(false);
            }
            openDraw.fillOval(centerPoint, centerPoint, cellSize, cellSize);
            openDraw.setFill(Color.YELLOW);
            openDraw.fillText("1", centerPoint + cellSize/2, centerPoint - 2);
        }
        else
        {
            int cellLabel = 1;
            for(double i = 0.0; i < 360; i += (360.0 / (this.getCellCount())))
            {

                if(userFault[cellLabel - 1])
                {
                    openDraw.setFill(Color.DARKGRAY);
                }
                else
                {
                    openDraw.setFill(Color.GREEN);
                }
                double pointX = centerPoint + (Math.cos(Math.toRadians(i + 270)) * centerPoint / 2);
                double pointY = centerPoint + (Math.sin(Math.toRadians(i + 270)) * centerPoint / 2);
                openDraw.fillOval(pointX, pointY, cellSize, cellSize);
                openDraw.setFill(Color.YELLOW);
                openDraw.fillText("" + cellLabel, pointX, pointY - 2);
                ++cellLabel;

            }
        }
    }

    /**
     * a representation to show that a cell is faulted. Also sets cell as faulted
     *
     * @param cellNo
     *         the cell number inside the tube
     */
    public void faultCell(int cellNo)
    {
        // get context of tube
        GraphicsContext drawingTube = this.getGraphicsContext2D();

        //cannot fault if running
        if(!canEdit){
            return;
        }

        //cannot make a fired cell faulted
        if(this.isFired.get(cellNo) || this.isFinished.get(cellNo))
        {
            return;
        }

        //draw the cell to show it has been set to faulted or not
        if(this.getFaultStatus(cellNo))
        {
            drawingTube.setFill(Color.GREEN);
            this.isFaulted.set(cellNo, false);
            userFault[cellNo] = false;
        }
        else
        {
            drawingTube.setFill(Color.GRAY);
            this.isFaulted.set(cellNo, true);
            userFault[cellNo] = true;
        }

        drawingTube.fillOval(cellLocation.get(cellNo * 2), cellLocation.get(cellNo * 2 + 1), cellSize, cellSize);

    }

    /**
     * the mouseEvent function logic that shows what cell is clicked on and the option to change to faulted
     *
     * @param mouseX
     *         = the horizontal location of the mouse click
     * @param mouseY
     *         = the vertical location of the mouse click
     */
    public void clickedOn(double mouseX, double mouseY)
    {
        if(!this.canEdit){
            return;
        }
        for(int cell = 0; cell < cellLocation.size(); cell += 2)
        {
            double testX = cellLocation.get(cell);
            double testY = cellLocation.get(cell + 1);
            if(testX - cellSize < mouseX && testX + cellSize > mouseX && testY - cellSize < mouseY && testY + cellSize > mouseY)
            {
                int guess = (cell / 2) + 1;
                this.faultCell(guess - 1);
            }
        }
    }


    /**
     * redraw the tube when the diagram panel is resize
     *
     * @param newSize the new size of the tube
     */
    public void redraw(double newSize) {
        this.setWidth(newSize);
        this.setHeight(newSize);
        this.tubePointXY = getWidth() / 6;
        this.tubeSize = getWidth() / 1.5;
        this.cellSize = getWidth()/15;
        this.centerPoint = (getWidth() / 2) - cellSize/2;

        GraphicsContext drawLabel = this.getGraphicsContext2D();
        drawLabel.clearRect(0,0, newSize, newSize);
        //draw tube hatch label
        drawLabel.setFill(Color.WHITE);
        drawLabel.setLineWidth(1d);
        drawLabel.setFont(Font.font("Verdana", cellSize));
        drawLabel.setFontSmoothingType(FontSmoothingType.LCD);
        drawLabel.fillText("Hatch #" + tubeNumber, 35d, getWidth() - 10);

        drawTube(this.cells);
    }
}
