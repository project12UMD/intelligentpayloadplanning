package GUI;

import DataHandlers.Constraint.CellStatuses;
import DataHandlers.PayloadList.PayloadList;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.util.function.Supplier;

/**
 * Displays some general info about the list.
 */
public class InfoPanel extends VBox implements UpdatablePanel
{
    private static final int WIDTH = 200;

    private static final String TITLE_TEXT = "Info";
    private static final String ACTION_TEXT = "Action Time: ";
    private static final String ARRIVAL_TEXT = "Arrival Time: ";
    private static final String TIME_TEXT = "Current Time: ";
    private static final String CELLS_TEXT = "Total Cells: ";
    private static final String AVAILABLE_TEXT = "Available Cells: ";

    private Supplier<CellStatuses> statusSupplier;
    private Label availableCellsLabel = new Label();
    private Label title = new Label(TITLE_TEXT);
    private Label line1 = new Label();
    private Label line2 = new Label();
    private ProgressBar timeBar = new ProgressBar();

    /**
     * Creates the info panel and sets style settings.
     *
     * @param statusSupplier
     *         The supplier used to get available cell information.
     */
    InfoPanel(Supplier<CellStatuses> statusSupplier)
    {
        this.statusSupplier = statusSupplier;
        this.getStyleClass()
            .add("root");
        this.getStylesheets()
            .add("global.css");
        setPrefWidth(WIDTH);
        title.setFont(Font.font(30));
        line1.setFont(Font.font(20));
        line2.setFont(Font.font(20));
        availableCellsLabel.setFont(Font.font(15));
    }

    /**
     * Displays the action and arrival time info for the given payload list.
     *
     * @param input
     *         The payload list whoes information will be displayed on the panel.
     */
    @Override
    public void update(PayloadList input)
    {
        line1.setText(ACTION_TEXT + input.getActionTime());
        line2.setText(ARRIVAL_TEXT + input.getArrivalTime());
        this.getChildren()
            .clear();
        this.getChildren()
            .addAll(title,
                    line1,
                    line2);
    }

    /**
     * Displays the current time.
     *
     * @param list
     *         The data the panel should be updated with.
     * @param time
     *         The current time.
     */
    @Override
    public void update(PayloadList list, int time)
    {
        line1.setText(TIME_TEXT + time);
        timeBar.setPrefSize(200, 50);
        this.getChildren()
            .clear();
        this.getChildren()
            .addAll(title,
                    line1,
                    timeBar);
    }

    /**
     * Displays the total amount of cells and the current amount of available cells.
     *
     * @param tubes
     *         The current amount of tubes.
     * @param cells
     *         The current amount of cells.
     */
    @Override
    public void update(int tubes, int cells)
    {
        line1.setText(CELLS_TEXT + (tubes * cells));
        this.getChildren()
            .clear();
        this.getChildren()
            .addAll(title,
                    line1,
                    availableCellsLabel);
        updateAvailableLabel();
    }

    /**
     * Updates the available cells label using the supplier already given to the info panel.
     */
    void updateAvailableLabel()
    {
        CellStatuses statuses = statusSupplier.get();
        int available = 0;
        for(int i = 0; i < statuses.getTubeAmount(); i++)
        {
            available += statuses.getAvailable(i);
        }
        availableCellsLabel.setText(AVAILABLE_TEXT + available);
    }

    /**
     * @return The progress bar that represents how far along in the simulation the application is.
     */
    ProgressBar getTimeBar()
    {
        return timeBar;
    }
}
