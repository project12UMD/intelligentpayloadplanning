package GUI.AppMenu;

import GUI.DiagramPanel.DiagramPanel;
import GUI.GanttChart.GanttChart;
import GUI.ListPanel.ListPanel;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.net.URL;

/**
 * the menu bar for the application
 */
public class AppMenu extends MenuBar{

    /**
     * enums for opening window
     */
    private enum Window {TUTORIAL, DIAGRAM, INPUT, LIST }
    public Stage ganttstage, tutorialWindow;
    public Menu help, window;
    public CheckMenuItem ganttWindow;
    public GanttPanel ganttCopy;
    private Label helpLabel, loadLabel;

    /**
     * constructor for the app menu
     *
     * @param panel the panel that will be used to display the gantt chart
     */
    public AppMenu(GanttPanel panel){
        ganttCopy = panel;
        window = new Menu("Window");
        ganttWindow = new CheckMenuItem("Gantt Chart");
        window.getItems().addAll(ganttWindow);
        ganttWindow.selectedProperty().addListener((e)->{
            if(ganttWindow.isSelected()){
                openWindow(Window.LIST);
            }
            else{
                if(ganttstage != null) ganttstage.close();
            }
        });

        help = new Menu();
        helpLabel = new Label("Help");
        loadLabel = new Label("Loading");
        helpLabel.setOnMouseClicked((e)->openWindow(Window.TUTORIAL));
        help.setGraphic(helpLabel);

        this.getMenus().addAll(window, help);
        this.getStyleClass().add("root");
        this.getStylesheets().add("global.css");

    }

    /**
     * the place where the dialog window will open
     *
     * @param setup the enum that says what option was clicked on
     */
    private void openWindow(Window setup) {

        switch(setup){
            case TUTORIAL:
                if(tutorialWindow == null){
                    help.setGraphic(loadLabel);
                    tutorialWindow = new Stage();
                    WebView htmlWindow = new WebView();
                    WebEngine engine = htmlWindow.getEngine();
                    URL link = getClass().getResource("generalTutorial.html");
                    engine.load(link.toExternalForm());
                    tutorialWindow.setTitle("tutorial");
                    Scene testScene = new Scene(htmlWindow, 900,500);
                    tutorialWindow.setScene(testScene);
                    tutorialWindow.show();
                    help.setGraphic(helpLabel);
                }
                else{
                   if(!tutorialWindow.isShowing()) tutorialWindow.show();
                }
                break;
            case LIST:
                openGanttWindow();
                break;
            default:
                System.out.println("reached default");
        }

    }

    /**
     *
     */
    private void openGanttWindow(){
        if (ganttstage != null) ganttstage.show();
        else{
        Scene scene = new Scene(ganttCopy);
        ganttstage = new Stage();
        ganttstage.setScene(scene);
        ganttstage.setTitle("Gantt Chart");
        ganttstage.show();
        ganttstage.setAlwaysOnTop(true);
        ganttstage.setOnCloseRequest((e)-> ganttWindow.setSelected(false));
        }
    }


}
