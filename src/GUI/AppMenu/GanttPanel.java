package GUI.AppMenu;

import DataHandlers.PayloadList.*;
import GUI.GanttChart.GanttChart;
import GUI.UpdatablePanel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.HashMap;
import java.util.Map;

/**
 * implementation of the gantt chart in another window
 */
public class GanttPanel extends Pane implements UpdatablePanel {

    Map<String, XYChart.Series<Number, String>> seriesList;
    private GanttChart<Number, String> chart;
    private NumberAxis xAxis;
    private CategoryAxis yAxis;

    /**
     * constructor for the panel
     */
    public GanttPanel(){
        this.getStyleClass().add("root");
        this.getStylesheets().add("global.css");

        xAxis = new NumberAxis();
        xAxis.setLabel("Time (sec)");
        xAxis.setTickLabelFill(Color.CHOCOLATE);
        xAxis.setMinorTickCount(5);
        xAxis.setTickUnit(1);
        xAxis.setTickMarkVisible(true);

        yAxis = new CategoryAxis();
        yAxis.setLabel("");
        yAxis.setTickLabelFill(Color.CHOCOLATE);
        yAxis.setTickLabelGap(10);
        yAxis.tickLabelFontProperty().set(Font.font(15));


        chart = new GanttChart<>(xAxis, yAxis);
        chart.setTitle("Deployment Actions");
        chart.setLegendVisible(false);
        chart.setBlockHeight(20d);

        chart.setMinSize(300d,300d);
        chart.setPrefSize(600d,600d);

        ScrollPane scrollHold = new ScrollPane(chart);

        getChildren().add(scrollHold);

        setMinSize(300d,300d);
        setPrefSize(600d,600d);
        setMaxSize(1000d,1000d);

        widthProperty().addListener((e)->{
            scrollHold.setPrefSize(getWidth() ,getHeight());
            chart.setPrefSize(getWidth() - 10,getHeight() - 10);
        });

        heightProperty().addListener((e)->{
            scrollHold.setPrefSize(getWidth() ,getHeight());
            chart.setPrefSize(getWidth()-10,getHeight()-10);
        });

    }
    /**
     * Updates the panel with the given payload list. Used for the output state of the application.
     *
     * @param list The data the panel should be updated with.
     */
    @Override
    public void update(PayloadList list) {
        if(seriesList != null)chart.getData().removeAll(seriesList.values());

        ObservableList<String> tubeID = FXCollections.observableArrayList();
        yAxis.setCategories(tubeID);
        for (int i = 1; i <= list.getConstraints().getTubeAmount(); i++)
        {
            tubeID.add("Tube " + i);
            for (int k = 1; k <= list.getConstraints().getCellAmount(); k++)
            {
                tubeID.add("Tube " + i + ", " + "Cell " + k);
            }
        }

        int numLabels = tubeID.size();
        double blockHeight = chart.getBlockHeight();
        chart.setMinHeight(numLabels * (blockHeight + yAxis.getTickLabelGap()) + 100);


        seriesList = new HashMap<>();
        for (Sequence sequence : list.getSequenceList())
        {
            for (DeploymentAction action : sequence.getUnmodifiableList())
            {
                int tube = action.getTube() + 1;
                int startTime = action.getStartTime();
                int actionTime = action.getActionTime();

                if (action instanceof HatchAction)
                {
                    HatchAction hatchAction = (HatchAction) action;
                    String actionID = "Tube " + tube;
                    if (hatchAction.getActionType() == HatchAction.TYPE.OPEN)
                    {
                        insertData(startTime, actionID, actionTime, "event-hatch-open");
                    }
                    else
                    {
                        insertData(startTime, actionID, actionTime, "event-hatch-close");
                    }
                }
                else
                {
                    PayloadAction payloadAction = (PayloadAction) action;
                    int cell = payloadAction.getCell() + 1;
                    int transitStart = startTime + actionTime;
                    int transitTime = payloadAction.getTarget().getTravelTime();
                    String actionID = "Tube " + tube + ", " + "Cell " + cell;

                    insertData(startTime, actionID, actionTime, "event-payload-launch");
                    insertData(transitStart, actionID, transitTime, "event-payload-arrival");
                }
            }
        }

    }

    /**
     * Updates the panel with the given payload list and has it represent the given time. Used during the simulation
     * state of the application.
     *
     * @param list The data the panel should be updated with.
     * @param time the current time in the payload simulation
     */
    @Override
    public void update(PayloadList list, int time) {
        chart.getData().removeAll(seriesList.values());


        ObservableList<String> tubeID = FXCollections.observableArrayList();

        yAxis.setCategories(tubeID);
        for (int i = 1; i <= list.getConstraints().getTubeAmount(); i++)
        {
            tubeID.add("Tube " + i);
            for (int k = 1; k <= list.getConstraints().getCellAmount(); k++)
            {
                tubeID.add("Tube " + i + ", " + "Cell " + k);
            }
        }

        int numLabels = tubeID.size();
        double blockHeight = chart.getBlockHeight();

        chart.setMinHeight(numLabels * (blockHeight +
                yAxis.getTickLabelGap()) + 100);

        seriesList = new HashMap<>();

        for (Sequence sequence : list.getSequenceList())
        {
            for (DeploymentAction action : sequence.getUnmodifiableList())
            {
                if(action.getStartTime() > time) continue;
                int tube = action.getTube() + 1;
                int startTime = action.getStartTime();
                int actionTime = action.getActionTime();

                if (action instanceof HatchAction)
                {
                    HatchAction hatchAction = (HatchAction) action;
                    String actionID = "Tube " + tube;
                    switch(hatchAction.getStatus(time)){
                        case OPENING:
                        case OPEN:
                            insertData(startTime, actionID,
                                    actionTime, "event-hatch-open");
                            break;
                        case WILL_CLOSE:
                        case CLOSING:
                        case CLOSED:
                            insertData(startTime, actionID,
                                    actionTime, "event-hatch-close");
                            break;
                        default:
                            //the other status types
                    }
                } else
                {
                    PayloadAction payloadAction = (PayloadAction) action;
                    int cell = payloadAction.getCell() + 1;
                    int transitStart = startTime + actionTime;
                    int transitTime = payloadAction.getTarget().getTravelTime();
                    String actionID = "Tube " + tube + ", " + "Cell " + cell;

                    //previous actions
                    insertData(startTime, actionID, actionTime, "event-payload-launch");

                    switch(payloadAction.getStatus(time)){
                        case LAUNCHING:
                            insertData(startTime, actionID,
                                    actionTime, "event-payload-launch");
                            break;
                        case IN_TRANSIT:
                            insertData(transitStart, actionID,
                                    transitTime, "event-payload-arrival");
                            break;
                        case ARRIVED:
                            insertData(transitStart, actionID, transitTime, "event-payload-arrival");
                            break;
                        default:
                            //someting else
                    }
                }
            }
        }
    }

    /**
     * Updates the panel with the current settings for tubes and cells. Used during the input state of the application.
     *
     * @param tubes The current amount of tubes.
     * @param cells The current amount of cells.
     */
    @Override
    public void update(int tubes, int cells) {

        if(seriesList != null)chart.getData().removeAll(seriesList.values());

        yAxis.getCategories().clear();

        ObservableList<String> tubeID = FXCollections.observableArrayList();
        yAxis.setCategories(tubeID);
        for (int i = 1; i <= tubes; i++)
        {
            tubeID.add("Tube " + i);
            for (int k = 1; k <= cells; k++)
            {
                tubeID.add("Tube " + i + ", " + "Cell " + k);
            }
        }



        int numLabels = tubeID.size();
        double blockHeight = chart.getBlockHeight();
        chart.setMinHeight(numLabels * (blockHeight + yAxis.getTickLabelGap()) + 100);
    }

    /**
     * @param startTime
     *      Start time of the Actions in the sequence list
     * @param actionID
     *      Id of the action whether it is tube or cell
     * @param actionTime
     *      Time it takes for the action to complete
     * @param style
     *      Color of the block for each action
     */
    private void insertData(int startTime, String actionID, int actionTime, String style)
    {
        XYChart.Series<Number, String> series;
        if (seriesList.containsKey(actionID))
        {
            series = seriesList.get(actionID);
        }
        else
        {
            series = new XYChart.Series<>();
            switch(style){
                case "event-payload-launch":
                    series.setName("Launched");
                    break;
                case "event-payload-arrival":
                    series.setName("Arrived");
                    break;
                case "event-hatch-open":
                    series.setName("Hatch Opens");
                    break;
                case "event-hatch-close":
                    series.setName("Hatch Closed");
                    break;
                default:
            }

            seriesList.put(actionID, series);
            chart.getData().add(series);
        }

        series.getData().add(
                new XYChart.Data<>(
                        startTime,
                        actionID,
                        new GanttChart.ExtraData(actionTime, style)
                )
        );
    }
}
