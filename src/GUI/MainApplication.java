package GUI;

import GUI.AppMenu.AppMenu;
import GUI.AppMenu.GanttPanel;
import GUI.DiagramPanel.DiagramPanel;
import GUI.InputPanel.InputPanel;
import GUI.ListPanel.ListPanel;
import Optimizer.BranchBoundOptimizer.BranchBoundOptimizer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * The main application for the program. Creates all the different panels for the GUI and fits them on the stage in
 * the correct locations.
 */
public class MainApplication extends Application
{
    /**
     * @param args
     *         TODO Add documentation.
     */
    public static void main(String[] args)
    {
        MainApplication.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        primaryStage.getIcons().add(new Image("GUI/tubeIcon.png"));
        primaryStage.setTitle("Intelligent Payload Planning Tool");
        VBox root = new VBox();
        root.getStyleClass().add("root");
        root.getStylesheets().add("global.css");

        ListPanel listPanel = new ListPanel();
        GanttPanel ganttWindow = new GanttPanel();
        DiagramPanel diagramPanel = new DiagramPanel();

        InfoPanel infoPanel = new InfoPanel(diagramPanel::fetchCellStatuses);
        diagramPanel.setOnEdit(infoPanel::updateAvailableLabel);
        InputPanel inputPanel = new InputPanel(diagramPanel::update, diagramPanel::fetchCellStatuses);
        inputPanel.setChangeHandler(() -> infoPanel.update(inputPanel.getConstraints()
                                                                     .getTubeAmount(), inputPanel.getConstraints()
                                                                                                 .getCellAmount()));
        // TODO find simulation with multiple different payload issue
        CommandPanel command = new CommandPanel(inputPanel, BranchBoundOptimizer::optimize);
        command.setOutputPanels(diagramPanel, infoPanel, listPanel, ganttWindow);
        inputPanel.setPrefHeight(600);
        inputPanel.setPrefWidth(340);

        AppMenu menu = new AppMenu(ganttWindow);

        root.getChildren().add(menu);
        root.getChildren()
            .add(new HBox());
        root.getChildren()
            .add(listPanel);

        ((HBox) root.getChildren()
                    .get(1)).getChildren()
                            .add(new VBox());
        ((VBox) ((HBox) root.getChildren()
                            .get(1)).getChildren()
                                    .get(0)).setMaxWidth(325.0);
        ((VBox) ((HBox) root.getChildren()
                            .get(1)).getChildren()
                                    .get(0)).setMinWidth(325.0);
        infoPanel.setMinWidth(150.0);
        infoPanel.setMaxWidth(150.0);
        ((VBox) ((HBox) root.getChildren()
                            .get(1)).getChildren()
                                    .get(0)).getChildren()
                                            .add(inputPanel);
        ((VBox) ((HBox) root.getChildren()
                            .get(1)).getChildren()
                                    .get(0)).getChildren()
                                            .add(command);
        ((HBox) root.getChildren()
                    .get(1)).getChildren()
                            .add(diagramPanel);
        ((HBox) root.getChildren()
                    .get(1)).getChildren()
                            .add(infoPanel);

        primaryStage.setScene(new Scene(root));
        primaryStage.setOnCloseRequest((e)-> System.exit(0));
        primaryStage.show();
    }
}
