package GUI;

import DataHandlers.PayloadList.PayloadList;

/**
 * Represents a panel that can be updated.
 */
public interface UpdatablePanel
{
    /**
     * Updates the panel with the given payload list. Used for the output state of the application.
     *
     * @param list
     *         The data the panel should be updated with.
     */
    void update(PayloadList list);

    /**
     * Updates the panel with the given payload list and has it represent the given time. Used during the simulation
     * state of the application.
     *
     * @param list
     *         The data the panel should be updated with.
     * @param time
     *         The time in seconds that have elapsed.
     */
    void update(PayloadList list, int time);

    /**
     * Updates the panel with the current settings for tubes and cells. Used during the input state of the application.
     *
     * @param tubes
     *         The current amount of tubes.
     * @param cells
     *         The current amount of cells.
     */
    void update(int tubes, int cells);
}
