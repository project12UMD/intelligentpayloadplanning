package Optimizer;

import DataHandlers.Constraint.Constraint;
import DataHandlers.PayloadList.PayloadList;

import java.util.function.Function;

/**
 * An interface for Payload Optimization functions. Simply a convinence version of the 'Function' FunctionalInterface.
 */
@FunctionalInterface
public interface OptimizationFunction extends Function<Constraint, PayloadList>
{
}
