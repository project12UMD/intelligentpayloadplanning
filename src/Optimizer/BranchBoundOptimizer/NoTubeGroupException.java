package Optimizer.BranchBoundOptimizer;

/**
 * An exception that represents when a Tube or Group transition is performed without there being any remaining Tubes
 * or Groups.
 */
class NoTubeGroupException extends IllegalArgumentException
{
    /**
     * @param message
     *         The message that should be passed to the error stack.
     */
    NoTubeGroupException(String message)
    {
        super(message);
    }
}
