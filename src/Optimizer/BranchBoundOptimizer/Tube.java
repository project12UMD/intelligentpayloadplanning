package Optimizer.BranchBoundOptimizer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Represents a tube, holds the tube number and the amount of available cells.
 */
class Tube
{
    private int id;
    private int availableCells;
    private List<CellGroup> cellGroupList;

    /**
     * Creates a Tube with the given settings.
     *
     * @param id
     *         The id of the Tube.
     * @param availableCells
     *         The available cells in the tube.
     * @param simultaneousCells
     *         The amount of cells that can be launched simultaneously from this tube.
     */
    Tube(int id, int availableCells, int simultaneousCells)
    {
        this.id = id;
        this.availableCells = availableCells;
        int cells = availableCells;
        cellGroupList = new ArrayList<>();

        while(cells / simultaneousCells != 0)
        {
            cellGroupList.add(new CellGroup(simultaneousCells));
            cells -= simultaneousCells;
        }
        if(cells % simultaneousCells != 0)
        {
            cellGroupList.add(new CellGroup(cells));
        }
    }

    /**
     * @return The id number for this tube.
     */
    int getId()
    {
        return id;
    }

    /**
     * @return The available cells in this tube.
     */
    int getAvailableCells()
    {
        return availableCells;
    }

    /**
     * @return A list of Integers that represents the amount of available cells in each group.
     */
    List<CellGroup> getGroupList()
    {
        return Collections.unmodifiableList(cellGroupList);
    }
}
