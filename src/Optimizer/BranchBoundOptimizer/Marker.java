package Optimizer.BranchBoundOptimizer;

import java.util.List;

/**
 * An immutable object that marks a specific spot in a list. Works similar to an Iterator that it allows forward
 * traversal of the list.
 */
class Marker<X>
{
    private final List<X> list;
    private final int index;

    /**
     * Creates a new Marker marking the first element of the given list.
     *
     * @param list
     *         The list the marker marks.
     */
    Marker(List<X> list)
    {
        this.list = list;
        index = 0;
    }

    /**
     * Creates a Marker for the given list at the given position.
     *
     * @param list
     *         The list to mark.
     * @param index
     *         The index of the marked element.
     */
    private Marker(List<X> list, int index)
    {
        this.list = list;
        this.index = index;
    }

    /**
     * @return The Marker for the next item in the list.
     */
    Marker<X> next()
    {
        if(!hasNextMarker())
        {
            throw new IllegalStateException("There is no next Marker.");
        }
        return new Marker<>(list, index + 1);
    }

    /**
     * @return The element that this marker marks.
     */
    X get()
    {
        return list.get(index);
    }

    /**
     * @return True if there is a marker after this one, false otherwise.
     */
    boolean hasNextMarker()
    {
        return index + 1 < list.size();
    }
}
