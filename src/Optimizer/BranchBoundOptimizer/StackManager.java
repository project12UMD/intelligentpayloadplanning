package Optimizer.BranchBoundOptimizer;

import DataHandlers.Constraint.Constraint;
import DataHandlers.Constraint.Location;
import DataHandlers.PayloadList.PayloadList;

/**
 * Manages the Node stack and ensures that the SequenceBin is properly updated at each step.
 */
class StackManager
{
    private Node currentNode;

    private boolean minNotSet = true;
    private SequenceBin bin;
    private PayloadList minList;
    private int minValue;
    private int goal;

    private Constraint constraints;

    /**
     * Creates a new manager for the node stack.
     *
     * @param constraints
     *         The constraints that go into this stack manager.
     */
    StackManager(Constraint constraints)
    {
        this.constraints = constraints;
        currentNode = new Node(constraints);
        bin = new SequenceBin(constraints);
        bin.add(currentNode);
        goal = 0;
        for(Location i : constraints.getLocations())
        {
            goal += i.getPayloadAmount();
        }
    }

    /**
     * @return The PayloadList representing the smallest value so far found.
     *
     * @throws IllegalStateException
     *         When a min list has not been found yet.
     */
    PayloadList getMinList()
    {
        if(minList == null)
        {
            throw new IllegalStateException("MinList not found.");
        }
        return minList;
    }

    /**
     * If it exists, adds the node representing the next tube to the stack. If it doesn'e exist, this method will do
     * nothing with the state of the stack manager.
     *
     * @return true if a node was added, false if not.
     */
    boolean nextTube()
    {
        if(currentNode.getTubeMarker()
                      .hasNextMarker())
        {
            currentNode = currentNode.nextTube();
            bin.add(currentNode);
            updateMin();
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * If it exists, adds the node representing the next group to the stack. If it doesn't exist, this method will do
     * nothing with the state of the stack manager.
     *
     * @return true if a node was added, false if not.
     */
    boolean nextGroup()
    {
        if(currentNode.getGroupMarker()
                      .hasNextMarker())
        {
            currentNode = currentNode.nextGroup();
            bin.add(currentNode);
            updateMin();
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Remove the latest node from the stack.
     */
    void remove()
    {
        if(currentNode.getPrevNode() != null)
        {
            bin.remove(currentNode);
            currentNode = currentNode.getPrevNode();
        }
    }

    /**
     * @return True if a possible better correct answer can be found by adding more nodes, false if not.
     */
    boolean continueSearch()
    {
        return enoughCellRemaining() && notEnoughLaunchedCells() && valueBetterThanMin();
    }

    /**
     * Check if the latest node is less than the min. Change out min if min is lower and enough cells have been
     * deployed.
     */
    private void updateMin()
    {
        if(currentNode.getLaunchedCells() >= goal && (minNotSet || bin.getValue() < minValue))
        {
            minValue = bin.getValue();
            minList = bin.getPayloadList();
            minNotSet = false;
        }
    }

    /**
     * @return True if there are enough cells left for a correct answer, false otherwise.
     */
    private boolean enoughCellRemaining()
    {
        return currentNode.getRemainingCells() + currentNode.getLaunchedCells() >= constraints.getNeededPayloads();
    }

    /**
     * @return True if there are not enough cells already launched, false otherwise.
     */
    private boolean notEnoughLaunchedCells()
    {
        return currentNode.getLaunchedCells() < constraints.getNeededPayloads();
    }

    /**
     * @return True if the current node is better than the current min list, false otherwise.
     */
    private boolean valueBetterThanMin()
    {
        return minNotSet || bin.getValue() < minValue;
    }
}

