package Optimizer.BranchBoundOptimizer;

/**
 * Represents a group of cells.
 */
class CellGroup
{
    private int cells;

    /**
     * Creates a new CellGroup with the given amount of cells.
     *
     * @param cells
     *         The amount of cells in the new group.
     */
    CellGroup(int cells)
    {
        this.cells = cells;
    }

    /**
     * @return The amount of cells in this group.
     */
    int getCells()
    {
        return cells;
    }
}
