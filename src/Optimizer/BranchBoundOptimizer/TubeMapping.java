package Optimizer.BranchBoundOptimizer;

import DataHandlers.Constraint.Constraint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Maps the "per cell" representation of the tubes to the "per group" representation of the tubes.
 */
class TubeMapping
{
    private List<Tube> tubeList;

    /**
     * Creates a new immutable mapping of the tubes. Two tube mappings created with the same constraints will
     * effectively be the same.
     *
     * @param input
     *         The constraints to create the mapping with.
     */
    TubeMapping(Constraint input)
    {
        tubeList = new ArrayList<>();
        for(int i = 0; i < input.getTubeAmount(); i++)
        {
            tubeList.add(new Tube(i, input.getAvailableCells(i), input.getDeploymentLimit()));
        }
        tubeList.sort(Comparator.comparingInt(Tube::getAvailableCells)
                                .reversed());
    }


    /**
     * Gets the amount of cells in the given group.
     *
     * @param tube
     *         The index of the tube.
     * @param group
     *         The index of the group in the tube.
     *
     * @return The amount of cells in this group.
     */
    int getCells(int tube, int group)
    {
        return tubeList.get(tube)
                       .getGroupList()
                       .get(group);
    }

    /**
     * Gets the amount of groups in the given tube.
     *
     * @param tube
     *         The index of the tube.
     *
     * @return The amount of groups in the given tube.
     */
    int getGroups(int tube)
    {
        return tubeList.get(tube)
                       .getAmountOfGroups();
    }

    /**
     * @return The amount of tubes represented in this mapping.
     */
    int TubeAmount()
    {
        return tubeList.size();
    }

    /**
     * @return The total available cells across all the tubes.
     */
    int getTotalCells()
    {
        int cells = 0;
        for(TubeMapping.Tube i : tubeList)
        {
            for(Integer j : i.getGroupList())
            {
                cells += j;
            }
        }
        return cells;
    }

    /**
     * Represents a tube, holds the tube number and the amount of available cells.
     */
    class Tube
    {
        private int id;
        private List<Integer> cellGroupList;

        /**
         * Creates a Tube with the given settings.
         *
         * @param id
         *         The id of the Tube.
         * @param availableCells
         *         The available cells in the tube.
         * @param simultaneousCells
         *         The amount of cells that can be launched simultaneously from this tube.
         */
        Tube(int id, int availableCells, int simultaneousCells)
        {
            this.id = id;
            int cells = availableCells;
            cellGroupList = new ArrayList<>();

            while(cells / simultaneousCells != 0)
            {
                cellGroupList.add(simultaneousCells);
                cells -= simultaneousCells;
            }
            if(cells % simultaneousCells != 0)
            {
                cellGroupList.add(cells);
            }
        }

        //TODO Method to be used in the future.

        /**
         * @return The id number for this tube.
         */
        int getId()
        {
            return id;
        }

        /**
         * @return The available cells in this tube.
         */
        int getAvailableCells()
        {
            int cells = 0;
            for(Integer i : cellGroupList)
            {
                cells += i;
            }
            return cells;
        }

        /**
         * @return The amount of groups in this tube.
         */
        int getAmountOfGroups()
        {
            return cellGroupList.size();
        }

        /**
         * @return A list of Integers that represents the amount of available cells in each group.
         */
        List<Integer> getGroupList()
        {
            return Collections.unmodifiableList(cellGroupList);
        }
    }

}
