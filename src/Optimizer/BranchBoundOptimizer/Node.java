package Optimizer.BranchBoundOptimizer;

import DataHandlers.Constraint.Constraint;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Represents the state of the stack at a particular point.
 */
class Node
{
    private Marker<Tube> tubeMarker;
    private Marker<CellGroup> groupMarker;

    private int value;
    private int launchedCells;
    private int remainingCells = 0;

    private Constraint constraints;
    private Node prevNode;

    /**
     * Creates the root node at tube 0, group 0.
     *
     * @param input
     *         The constraints of the given system.
     */
    Node(Constraint input)
    {
        List<Tube> tubeList = generateTubeList(input);
        tubeMarker = new Marker<>(tubeList);
        groupMarker = new Marker<>(tubeMarker.get()
                                             .getGroupList());
        value = input.getDeploymentTime();
        launchedCells = groupMarker.get()
                                   .getCells();
        for(Tube tube : tubeList)
        {
            remainingCells += tube.getAvailableCells();
        }
        remainingCells -= launchedCells;
        constraints = input;
    }

    /**
     * Creates a branch node at the given tube and group.
     *
     * @param prevNode
     *         The previous node in the stack.
     * @param tubeMarker
     *         The tube to branch too.
     * @param groupMarker
     *         The group to branch too.
     */
    private Node(Node prevNode, Marker<Tube> tubeMarker, Marker<CellGroup> groupMarker)
    {
        constraints = prevNode.constraints;
        this.prevNode = prevNode;
        this.groupMarker = groupMarker;
        this.tubeMarker = tubeMarker;
        value = calculateValue();
        launchedCells = calculateLaunchedCells();
        remainingCells = calculateRemainingCells();
    }

    /**
     * Generates an ordered Tube list based on the given constraints.
     *
     * @param input
     *         The constraints to build the tube list.
     *
     * @return A list of Tubes in order of their available cells.
     */
    private static List<Tube> generateTubeList(Constraint input)
    {
        List<Tube> tubeList = new ArrayList<>();
        for(int i = 0; i < input.getTubeAmount(); i++)
        {
            tubeList.add(new Tube(i, input.getAvailableCells(i), input.getDeploymentLimit()));
        }
        tubeList.sort(Comparator.comparingInt(Tube::getAvailableCells)
                                .reversed());
        return tubeList;
    }

    /**
     * Creates a new node at the next group of the given node.
     *
     * @return The new node at the new group if it exists, null otherwise.
     */
    Node nextGroup()
    {
        return new Node(this, tubeMarker, groupMarker.next());
    }

    /**
     * Creates a new node at the next tube of the given node.
     *
     * @return The new tube at the new tube if it exists, null otherwise.
     */
    Node nextTube()
    {
        Marker<Tube> next = tubeMarker.next();
        return new Node(this, next, new Marker<>(next.get()
                                                     .getGroupList()));
    }

    /**
     * @return The total value of the node.
     */
    int getValue()
    {
        return value;
    }

    /**
     * @return The amount of launched cells.
     */
    int getLaunchedCells()
    {
        return launchedCells;
    }

    /**
     * @return The amount of cells remaining.
     */
    int getRemainingCells()
    {
        return remainingCells;
    }

    /**
     * @return The tube of this node.
     */
    Tube getTube()
    {
        return tubeMarker.get();
    }

    /**
     * @return The marker used to mark the current group of the Node.
     */
    Marker<CellGroup> getGroupMarker()
    {
        return groupMarker;
    }

    /**
     * @return The marker used to mark the current tube of the Node.
     */
    Marker<Tube> getTubeMarker()
    {
        return tubeMarker;
    }

    /**
     * @return The group of this node.
     */
    CellGroup getGroup()
    {
        return groupMarker.get();
    }

    /**
     * @return The previous node in the stack.
     */
    Node getPrevNode()
    {
        return prevNode;
    }

    /**
     * Calculates the total value of the given node.
     *
     * @return The total value of the new node.
     */
    private int calculateValue()
    {
        return constraints.getDeploymentTime();
    }

    /**
     * Calculates the total amount of launched cells.
     *
     * @return The total amount of launched cells in the new tube.
     */
    private int calculateLaunchedCells()
    {
        return prevNode.getLaunchedCells() + groupMarker.get()
                                                        .getCells();
    }

    /**
     * @return The total amount of remaining cells available.
     */
    private int calculateRemainingCells()
    {
        int reduction = groupMarker.get()
                                   .getCells();
        if(prevNode.getTube() != tubeMarker.get())
        {
            Marker<CellGroup> current = prevNode.getGroupMarker();
            while(current.hasNextMarker())
            {
                current = current.next();
                reduction += current.get()
                                    .getCells();
            }
        }
        return prevNode.getRemainingCells() - reduction;
    }
}