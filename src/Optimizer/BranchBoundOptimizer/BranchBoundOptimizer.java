package Optimizer.BranchBoundOptimizer;

import DataHandlers.Constraint.Constraint;
import DataHandlers.PayloadList.PayloadList;

/**
 * An optimizer that uses a binary search tree to exhaustively search all available options for the smallest time.
 */
public class BranchBoundOptimizer
{

    /**
     * This class is a static utility class and cannot be created.
     */
    private BranchBoundOptimizer()
    {
    }

    /**
     * Creates an optimal PayloadList using the branch bound method.
     *
     * @param constraints
     *         The constraints of the list.
     *
     * @return An optimized PayloadList.
     */
    public static PayloadList optimize(Constraint constraints)
    {
        StackManager stackManager = new StackManager(constraints);
        search(stackManager);
        return stackManager.getMinList();
    }

    /**
     * Recursively searches the Binary Tree for an optimal solution.
     *
     * @param stackManager
     *         The stack manager to be used for the search.
     */
    private static void search(StackManager stackManager)
    {
        if(stackManager.continueSearch())
        {
            if(stackManager.nextTube())
            {
                search(stackManager);
                stackManager.remove();
            }
            if(stackManager.nextGroup())
            {
                search(stackManager);
                stackManager.remove();
            }
        }
    }
}
