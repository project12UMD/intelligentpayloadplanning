package Optimizer.BranchBoundOptimizer;

import DataHandlers.Constraint.Constraint;
import DataHandlers.Constraint.Location;
import DataHandlers.PayloadList.PayloadList;
import DataHandlers.PayloadList.PayloadListBuilder;
import org.jetbrains.annotations.Nullable;

import java.util.*;

/**
 * Keeps track of the value of the sequences and which nodes are in which sequence.
 */
class SequenceBin
{
    private List<Sequence> sequences;
    private Map<Node, Sequence> sequenceMap;
    private Constraint constraints;

    /**
     * Constructor used to create a new empty sequence bin.
     *
     * @param constraints
     *         The constraints used to create this bin.
     */
    SequenceBin(Constraint constraints)
    {
        this.constraints = constraints;
        sequences = new ArrayList<>();
        for(int i = 0; i < constraints.getHatchLimit(); i++)
        {
            sequences.add(new Sequence());
        }
        sequenceMap = new HashMap<>();
    }

    /**
     * @return The value of this bin which is equivalent to the value of the largest sequence.
     */
    int getValue()
    {
        int value = 0;
        for(Sequence i : sequences)
        {
            if(i.value() > value)
            {
                value = i.value();
            }
        }
        return value;
    }

    /**
     * Add the node to the bin.
     *
     * @param node
     *         The node to be added.
     */
    void add(Node node)
    {
        if(node.getPrevNode() == null || node.getPrevNode()
                                             .getTube() != node.getTube())
        {
            sequenceMap.put(node, getMin());
        }
        else
        {
            sequenceMap.put(node, sequenceMap.get(node.getPrevNode()));
        }
        sequenceMap.get(node)
                   .add(node);
    }

    /**
     * Removes the node from the bin
     *
     * @param node
     *         The node to be removed.
     */
    void remove(Node node)
    {
        sequenceMap.get(node)
                   .remove();
        sequenceMap.remove(node);
    }

    /**
     * Takes the current status of the bin and creates a new PayloadList.
     *
     * @return The created PayloadList.
     */
    PayloadList getPayloadList()
    {
        Iterator<Location> currentLocation = getLocationIterator();

        PayloadListBuilder builder = new PayloadListBuilder(constraints);

        int currentSequence = 0;
        for(Sequence sequence : sequences)
        {
            builder.sequence(currentSequence);
            currentSequence++;
            Tube currentTube = null;
            Integer lastCell = null;

            for(Node j : sequence.getList())
            {
                if(currentTube == null || currentTube != j.getTube())
                {
                    currentTube = j.getTube();
                    builder.tube(currentTube.getId());
                    lastCell = null;
                }

                for(int k = 0; k < j.getGroup()
                                    .getCells() && currentLocation.hasNext(); k++)
                {
                    lastCell = nextAvailableCell(lastCell, j.getTube());
                    System.out.println("");
                    builder.deploy(lastCell, currentLocation.next());
                }
            }
        }
        return builder.build();
    }

    /**
     * Used to create the final payload list.
     *
     * @return An Iterator for all the needed payloads sorted by travel time. There will be one location object per
     * needed payload.
     */
    private Iterator<Location> getLocationIterator()
    {
        List<Location> locationList = new ArrayList<>();
        for(Location i : constraints.getLocations())
        {
            for(int j = 0; j < i.getPayloadAmount(); j++)
            {
                locationList.add(i);
            }
        }
        locationList.sort(Comparator.comparingInt(Location::getTravelTime)
                                    .reversed());
        return locationList.iterator();
    }


    /**
     * @return The sequence with the smallest current value.
     */
    private Sequence getMin()
    {
        Sequence min = sequences.get(0);
        for(Sequence i : sequences)
        {
            if(i.value() < min.value())
            {
                min = i;
            }
        }
        return min;
    }

    /**
     * Used by getPayloadList() to create a new PayloadList. Calculates what the next cell to deploy next given what
     * cells are faulted. Prevents faulted cells from being launched from.
     *
     * @param lastCell
     *         The cell that was last launched to. Null means that this is the first cell.
     * @param tube
     *         The tube to search.
     *
     * @return The next available cell to launch from.
     *
     * @throws IllegalArgumentException
     *         When the tube is less than zero or greater than or equal to the amount of tubes.
     * @throws IllegalArgumentException
     *         When lastCell is less than zero or equal to or greater than the amount of cells in the tube.
     * @throws IllegalStateException
     *         When there is no available cells left.
     */
    private int nextAvailableCell(@Nullable Integer lastCell, Tube tube)
    {
        if(tube == null)
        {
            throw new IllegalArgumentException("Tube cannot be null.");
        }
        if(lastCell != null && (lastCell < 0 || lastCell >= constraints.getCellAmount()))
        {
            throw new IllegalArgumentException("cell must be non negative and less than the amount of cells.");
        }
        boolean continueSearch = true;
        do
        {
            if(lastCell == null)
            {
                lastCell = 0;
            }
            else
            {
                lastCell++;
            }
            //Suppress possible null pointer exception. Resolved from the previous if statement.
            //noinspection ConstantConditions
            if(constraints.getCellStatus(tube.getId(), lastCell))
            {
                continueSearch = false;
            }
            else if(lastCell >= constraints.getCellAmount())
            {
                throw new IllegalStateException("No additional cells available");
            }
        } while(continueSearch);
        return lastCell;
    }


    /**
     * Represents a sequence of actions that can be taken by the final PayloadList.
     */
    class Sequence
    {
        private List<Node> list;
        private int transitions = 0;

        /**
         * Creates a new Sequence.
         */
        Sequence()
        {
            list = new ArrayList<>();
        }

        /**
         * Adds a new node to the end of this sequence.
         *
         * @param node
         *         The node to be added.
         */
        public void add(Node node)
        {
            if(list.size() != 0 && list.get(list.size() - 1)
                                       .getTube() != node.getTube())
            {
                transitions++;
            }
            list.add(node);
        }

        /**
         * removes the given node from the list.
         *
         * @throws IllegalArgumentException
         *         When the given node isn't the last in the list.
         */
        public void remove()
        {
            if(list.size() > 1 && list.get(list.size() - 1)
                                      .getTube() != list.get(list.size() - 2)
                                                        .getTube())
            {
                transitions--;
            }
            list.remove(list.size() - 1);
        }

        /**
         * @return An unmodifiable list of all nodes in this sequence.
         */
        public List<Node> getList()
        {
            return Collections.unmodifiableList(list);
        }

        /**
         * @return The current size of this sequence.
         */
        public int size()
        {
            return list.size();
        }

        /**
         * @return The current value of this sequence.
         */
        int value()
        {
            int total = 0;
            for(Node i : list)
            {
                total += i.getValue();
            }
            total += transitions * constraints.getHatchTime() * 2;
            return total;
        }
    }

}
