package DataHandlers.Constraint;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * An immutable object that holds all the constraints for the system. Constraint objects can be made through using the
 * provided builder. Having the constraint immutable gurentees that after the constraints are set for the system that
 * they will not change.
 */
public class Constraint
{
    private static final int DEFAULT_AMOUNT = 1;
    private static final int DEFAULT_LIMIT = 1;
    private static final int DEFAULT_TIME = 0;

    private int tubeAmount = DEFAULT_AMOUNT;
    private int cellAmount = DEFAULT_AMOUNT;
    private List<Location> locationList = new ArrayList<>();
    private int hatchLimit = DEFAULT_LIMIT;
    private int payloadTime = DEFAULT_TIME;
    private int deploymentLimit = DEFAULT_LIMIT;
    private int hatchTime = DEFAULT_TIME;
    private CellStatuses statuses;

    /**
     * @return The amount of available tubes. The amount of tubes will be greater than or equal to zero.
     */
    public int getTubeAmount()
    {
        return tubeAmount;
    }

    /**
     * @return The amount of cells in each tube. The amount will be greater than or equal to zero.
     */
    public int getCellAmount()
    {
        return cellAmount;
    }

    /**
     * Retrieves an unmodifiable list of all the locations. This includes the data about how many payloads that
     * need to be deployed and the travel time of those payloads.
     *
     * @return An unmodifiable list of payloads.
     */
    public List<Location> getLocations()
    {
        return Collections.unmodifiableList(locationList);
    }

    /**
     * @return The amount of payloads needed to launch.
     */
    public int getNeededPayloads()
    {
        int total = 0;
        for(Location i : locationList)
        {
            total += i.getPayloadAmount();
        }
        return total;
    }

    /**
     * @return The amount of hatches that can be open at one time. This number will be greater or equal to zero.
     */
    public int getHatchLimit()
    {
        return hatchLimit;
    }

    /**
     * @return The amount of time in seconds that it will take to launch a payload. This number will be greater than or
     * equal to zero.
     */
    public int getDeploymentTime()
    {
        return payloadTime;
    }

    /**
     * @return The amount of simultaneous payload deployments per tube. This number will be greater than or equal to
     * zero.
     */
    public int getDeploymentLimit()
    {
        return deploymentLimit;
    }

    /**
     * @return The amount of time in seconds for a hatch to open. The number of will be greater than or equal to zero.
     */
    public int getHatchTime()
    {
        return hatchTime;
    }

    /**
     * Gets the status of the given cell.
     *
     * @param tube
     *         The tube that the cell is in.
     * @param cell
     *         The cell to be checked.
     *
     * @return True if the cell is available, false otherwise.
     *
     * @throws IllegalArgumentException
     *         When tube or cell is less than zero or if tube is greater than or equal to the amount of tubes or cell is
     *         greater than or equal to the amount of cells.
     */
    public boolean getCellStatus(int tube, int cell)
    {
        if(tube < 0 || cell < 0)
        {
            throw new IllegalArgumentException("Tube and Cell may not be less than zero. Tube:" + tube + ", Cell:" +
                                                       cell);
        }
        if(tube >= tubeAmount || cell >= cellAmount)
        {
            throw new IllegalArgumentException("Tube or Cell is greater than the amount of available tubes or Cells" +
                                                       ".\n " +
                                                       "Tube:" + tube + " Tube Amount:" + tubeAmount + "; Cell:" +
                                                       cell + " Cell Amount:" + cellAmount);
        }
        return statuses.getStatus(tube, cell);
    }

    /**
     * Gets the amount of available cells in the given tube.
     *
     * @param tube
     *         The tube to check.
     *
     * @return The number of available cells in the tube.
     */
    public int getAvailableCells(int tube)
    {
        return statuses.getAvailable(tube);
    }

    /**
     * Creates immutable Constraint objects. Setting calls in this builder can be strung together like so:
     * <p/>
     * (new ConstraintHandler()).setTubeAmount(x).setCellAmount(x).setHatchLimit(x).optimize();
     */
    public static class Builder
    {
        private Constraint currentSettings = new Constraint();

        /**
         * Sets the amount of tubes.
         *
         * @param tubeAmount
         *         The value to be set.
         *
         * @return this.
         *
         * @throws IllegalArgumentException
         *         The input must not be less than zero.
         */
        public Builder setTubeAmount(int tubeAmount)
        {
            if(tubeAmount < 1)
            {
                throw new IllegalArgumentException("Input value less than zero");
            }
            currentSettings.tubeAmount = tubeAmount;
            return this;
        }

        /**
         * Sets the amount of cells in each tube.
         *
         * @param cellAmount
         *         The value to be set.
         *
         * @return this.
         *
         * @throws IllegalArgumentException
         *         The input must not be less than one.
         */
        public Builder setCellAmount(int cellAmount)
        {
            if(cellAmount < 1)
            {
                throw new IllegalArgumentException("Input value less than one");
            }
            currentSettings.cellAmount = cellAmount;
            return this;
        }

        /**
         * Creates a new Location object and adds it to the list of locations. Location objects will get sequential ids
         * starting with 1.
         *
         * @param amountOfPayloads
         *         The amount of payloads that need to go to that location.
         * @param travelTime
         *         The amount of time in seconds it takes for a payload to arrive at the target.
         *
         * @return this.
         *
         * @throws IllegalArgumentException
         *         The amount of payloads must be greater than 0 and the travel time must not be less than 0.
         */
        public Builder addLocation(int amountOfPayloads, int travelTime)
        {
            if(amountOfPayloads < 1 || travelTime < 0)
            {
                throw new IllegalArgumentException("Amount of payloads must be greater than 0 and Travel Time must be" +
                                                           "greater than or equal to 0");
            }
            currentSettings.locationList.add(new Location(currentSettings.locationList.size() + 1,
                                                          amountOfPayloads,
                                                          travelTime));
            return this;
        }

        /**
         * Sets the amount of hatches that can be open at one time.
         *
         * @param hatchLimit
         *         The value to set.
         *
         * @return this.
         *
         * @throws IllegalArgumentException
         *         The input must not be less than one.
         */
        public Builder setHatchLimit(int hatchLimit)
        {
            if(hatchLimit < 1)
            {
                throw new IllegalArgumentException("Input value less than one");
            }
            currentSettings.hatchLimit = hatchLimit;
            return this;
        }

        /**
         * Sets the time in seconds it takes to launch a payload.
         *
         * @param payloadTime
         *         The value to set.
         *
         * @return this.
         *
         * @throws IllegalArgumentException
         *         The input must not be less than zero.
         */
        public Builder setDeploymentTime(int payloadTime)
        {
            if(payloadTime < 0)
            {
                throw new IllegalArgumentException("Input value less than zero");
            }
            currentSettings.payloadTime = payloadTime;
            return this;
        }

        /**
         * Sets the amount of payloads that can be deployed at a time.
         *
         * @param deploymentLimit
         *         The value to be set.
         *
         * @return this.
         *
         * @throws IllegalArgumentException
         *         The input must not be less than one.
         */
        public Builder setDeploymentLimit(int deploymentLimit)
        {
            if(deploymentLimit < 1)
            {
                throw new IllegalArgumentException("Input value less than one");
            }
            currentSettings.deploymentLimit = deploymentLimit;
            return this;
        }

        /**
         * Sets the time in seconds for the hatch to open or close.
         *
         * @param hatchTime
         *         The value to be set.
         *
         * @return this.
         *
         * @throws IllegalArgumentException
         *         The input must not be less than zero.
         */
        public Builder setHatchTime(int hatchTime)
        {
            if(hatchTime < 0)
            {
                throw new IllegalArgumentException("Input value less than zero");
            }
            currentSettings.hatchTime = hatchTime;
            return this;
        }

        /**
         * Builds a constraint object with the given settings. The cells will all be set to available.
         *
         * @return The newly built immutable Constraint object.
         */
        public Constraint build()
        {
            currentSettings.statuses = new CellStatuses(currentSettings.getTubeAmount(),
                                                        currentSettings.getCellAmount());
            Constraint temp = currentSettings;
            currentSettings = new Constraint();
            return temp;
        }

        /**
         * Builds a constraint object with the given settings. The cell statuses will be set to the given statuses.
         *
         * @param statuses
         *         The current cell statuses.
         *
         * @return A newly built immutable Constraint object with the given cell statuses set.
         */
        public Constraint build(CellStatuses statuses)
        {
            currentSettings.statuses = statuses;
            Constraint temp = currentSettings;
            currentSettings = new Constraint();
            return temp;
        }
    }
}