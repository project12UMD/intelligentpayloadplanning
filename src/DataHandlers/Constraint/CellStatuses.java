package DataHandlers.Constraint;

import java.util.ArrayList;
import java.util.List;

/**
 * A list that contains the statuses of all the cells.
 */
public class CellStatuses
{
    private List<List<Boolean>> statuses;

    /**
     * Creates a new list of statuses that are all set to available.
     *
     * @param tubes
     *         The amount of tubes.
     * @param cells
     *         The amount of cells.
     */
    public CellStatuses(int tubes, int cells)
    {
        statuses = new ArrayList<>();
        for(int i = 0; i < tubes; i++)
        {
            statuses.add(new ArrayList<>());
            for(int j = 0; j < cells; j++)
            {
                statuses.get(i)
                        .add(true);
            }
        }
    }

    /**
     * @return The amount of tubes.
     */
    public int getTubeAmount()
    {
        return statuses.size();
    }

    /**
     * @return The amount of cells.
     */
    public int getCellAmount()
    {
        return statuses.get(0)
                       .size();
    }

    /**
     * Sets the status of a cell.
     *
     * @param tube
     *         The tube the cell is in.
     * @param cell
     *         The cell to be set.
     * @param status
     *         The status of the cell, true is available, false is not.
     */
    public void setStatus(int tube, int cell, boolean status)
    {
        statuses.get(tube)
                .set(cell, status);
    }

    /**
     * Gets the amount of cells available in a given tube.
     *
     * @param tube
     *         The tube to check.
     *
     * @return The amount of available cells.
     */
    public int getAvailable(int tube)
    {
        int available = 0;
        for(Boolean status : statuses.get(tube))
        {
            if(status)
            {
                available++;
            }
        }
        return available;
    }

    /**
     * Gets the status of a cell.
     *
     * @param tube
     *         The tube the cell is in.
     * @param cell
     *         The cell status to be retrieved.
     *
     * @return The status, true is available, false is not.
     */
    Boolean getStatus(int tube, int cell)
    {
        return statuses.get(tube)
                       .get(cell);
    }
}
