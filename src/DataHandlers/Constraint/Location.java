package DataHandlers.Constraint;

/**
 * Simple object that represents a location.
 */
public class Location
{
    private int payloadAmount;
    private int travelTime;
    private int id;

    /**
     * Creates a new immutable Location object with the given settings.
     *
     * @param id
     *         The id for this new location.
     * @param payloadAmount
     *         The amount of payloads that need to be sent to this location.
     * @param travelTime
     *         The amount of time it takes to travel to this location.
     */
    public Location(int id, int payloadAmount, int travelTime)
    {
        if(travelTime < 0)
        {
            throw new IllegalArgumentException("Travel time must not be less than zero.");
        }
        if(payloadAmount < 1)
        {
            throw new IllegalArgumentException("Payload amount must be greater than or equal to 1");
        }
        this.payloadAmount = payloadAmount;
        this.travelTime = travelTime;
        this.id = id;
    }

    /**
     * @return The amount of payloads that need to be launched at this location.
     */
    public int getPayloadAmount()
    {
        return payloadAmount;
    }

    /**
     * @return The amount of time it takes to travel to this location.
     */
    public int getTravelTime()
    {
        return travelTime;
    }

    /**
     * @return The id number for this location.
     */
    public int getId()
    {
        return id;
    }
}
