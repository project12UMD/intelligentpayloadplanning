/**
 * Holds all classes needed to create payload lists. The payload list is a list of actions that need to be taken in
 * order to complete a mission.
 */
package DataHandlers.PayloadList;