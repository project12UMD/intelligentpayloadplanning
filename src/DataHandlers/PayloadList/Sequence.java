package DataHandlers.PayloadList;

import DataHandlers.Constraint.Constraint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Represents a sequence of deployment actions that will occur sequentially.
 */
public class Sequence
{
    private Constraint constraints;
    private List<DeploymentAction> actionList = new ArrayList<>();
    private int totalTime = 0;
    private int arrivalTime = 0;
    private int cellCount = 1;

    /**
     * Creates a new Sequence.
     *
     * @param constraints
     *         The constraints that should go into this sequence.
     */
    Sequence(Constraint constraints)
    {
        this.constraints = constraints;
    }

    /**
     * @return The last action in the sequence.
     *
     * @throws IllegalStateException
     *         When the sequence is empty.
     */
    DeploymentAction getLatestAction()
    {
        if(actionList.size() == 0)
        {
            throw new IllegalStateException("Sequence is empty.");
        }
        return actionList.get(actionList.size() - 1);
    }

    /**
     * @return The total action time of this sequence.
     */
    int getActionTime()
    {
        return totalTime;
    }

    /**
     * @return The total arrival time of this sequence.
     */
    int getArrivalTime()
    {
        return arrivalTime;
    }

    /**
     * @return Gets the list of all deployment actions in an unmodifiable list.
     */
    public List<DeploymentAction> getUnmodifiableList()
    {
        return Collections.unmodifiableList(actionList);
    }

    /**
     * Adds the given PayloadAction to the end of this sequence.
     *
     * @param newAction
     *         The action to add to this sequence.
     *
     * @return this.
     */
    Sequence addAction(PayloadAction newAction)
    {

        if(cellCount % constraints.getDeploymentLimit() == 0)
        {
            cellCount++;
            newAction.setStartTime(totalTime);
            totalTime += newAction.getActionTime();
            arrivalTime = newAction.getArrivalTime();
        }
        else
        {
            cellCount++;
            newAction.setStartTime(totalTime - constraints.getDeploymentTime());
        }
        actionList.add(newAction);
        return this;
    }

    /**
     * Adds the given HatchAction to the end of this sequence.
     *
     * @param newAction
     *         The action to add to this sequence.
     *
     * @return this.
     */
    Sequence addAction(HatchAction newAction)
    {
        newAction.setStartTime(totalTime);
        actionList.add(newAction);
        totalTime += newAction.getActionTime();
        cellCount = 0;
        return this;
    }

    /**
     * @return true if this Sequence has an element, false if it is empty.
     */
    boolean hasElement()
    {
        return actionList.size() != 0;
    }
}
