package DataHandlers.PayloadList;

/**
 * A enum that represents the current status of a cell.
 */
public enum CellStatus
{
    OPEN, LAUNCHED, UNAVAILABLE, FAULTED, LAUNCHING
}
