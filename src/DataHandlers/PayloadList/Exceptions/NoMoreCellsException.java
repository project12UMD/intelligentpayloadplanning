package DataHandlers.PayloadList.Exceptions;

/**
 * Signals that there are no more available cells to be launched.
 */
public class NoMoreCellsException extends IllegalStateException
{
    /**
     * @param message
     *         The message that should be sent to the error stack.
     */
    public NoMoreCellsException(String message)
    {
        super(message);
    }
}
