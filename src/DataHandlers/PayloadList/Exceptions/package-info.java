/**
 * Holds special exceptions that are used by the PayloadList. All these exceptions extend IllegalStateException so they
 * can be all caught by catching that exception. These special exceptions were created so that each kind of error may
 * be handled separately if so desired.
 */
package DataHandlers.PayloadList.Exceptions;