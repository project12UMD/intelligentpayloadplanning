package DataHandlers.PayloadList.Exceptions;

/**
 * Signals that the PayloadBuilder is in an incorrect state for the method to be called. This could mean that the
 * constraints were not set before calling sequence or that deploy was called before tube.
 */
public class BuilderStateException extends IllegalStateException
{
    /**
     * @param message
     *         The message to be passed to the error stack.
     */
    public BuilderStateException(String message)
    {
        super(message);
    }
}
