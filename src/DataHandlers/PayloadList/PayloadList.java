package DataHandlers.PayloadList;

import DataHandlers.Constraint.Constraint;

import java.util.*;

/**
 * An unmutable list of payload deployments. Must be built with the provided builder class.
 */
public class PayloadList
{
    private List<Sequence> sequenceList;
    private CellStatus[][] cellStatuses;
    private Constraint constraints;
    private int actionTime;
    private int arrivalTime;

    /**
     * A package private constructor that is only to be called by the builder to build a PayloadList.
     *
     * @param builder
     *         The builder that will be used to construct the PayloadList.
     */
    PayloadList(PayloadListBuilder builder)
    {
        sequenceList = builder.getSequenceList();
        this.constraints = builder.getConstraints();
        actionTime = 0;
        arrivalTime = 0;
        for(Sequence i : sequenceList)
        {
            actionTime = (i.getActionTime() > actionTime) ? i.getActionTime() : actionTime;
            arrivalTime = (i.getArrivalTime() > arrivalTime) ? i.getArrivalTime() : arrivalTime;
        }
        cellStatuses = new CellStatus[constraints.getTubeAmount()][constraints.getCellAmount()];
        for(int i = 0; i < constraints.getTubeAmount(); i++)
        {
            Arrays.fill(cellStatuses[i], CellStatus.OPEN);
        }
        for(int i = 0; i < constraints.getTubeAmount(); i++)
        {
            for(int j = 0; j < constraints.getCellAmount(); j++)
            {
                if(builder.getCellsLaunched()[i][j])
                {
                    cellStatuses[i][j] = CellStatus.LAUNCHED;
                }
                else if(!constraints.getCellStatus(i, j))
                {
                    cellStatuses[i][j] = CellStatus.UNAVAILABLE;
                }
            }
        }
    }

    /**
     * Gets a list of all the deployment actions in the list ordered by their start time. This can be used to represent
     * the list in a serial format. Does not accurately reflect simultaneous deployments and hatches.
     *
     * @return An unmodifiable ordered list of deployment actions.
     */
    public List<DeploymentAction> getOrderList()
    {
        List<DeploymentAction> orderList = new ArrayList<>();
        for(Sequence i : sequenceList)
        {
            for(DeploymentAction j : i.getUnmodifiableList())
            {
                orderList.add(j);
            }
        }
        orderList.sort(Comparator.comparingInt(DeploymentAction::getStartTime));
        return Collections.unmodifiableList(orderList);
    }

    /**
     * Gets the amount of time in seconds it will take to go through all the actions in the list. This does not take
     * into account arrival times for deployments.This is equivalent to the action time of the longest sequence.
     *
     * @return The time in seconds it takes to go through all the actions in the list.
     */
    public int getActionTime()
    {
        return actionTime;
    }

    /**
     * Gets the total amount of time in seconds it will take for all the actions to be complete and all deployments
     * to reach their targets. This is equivalent of the largest arrival time of all the sequences.
     *
     * @return The amount of time in seconds.
     */
    public int getArrivalTime()
    {
        return arrivalTime;
    }

    /**
     * @return The status of the cells at the completion of this PayloadList.
     */
    public CellStatus[][] getCellStatuses()
    {
        return cellStatuses;
    }

    /**
     * This returns a list of sequences that hold all the actions in the list. The list returned will be unmodifiable
     * and will throw an exception if it is modified. The sequences are treated as if they are run in parallel.
     *
     * @return An unmodifiable list of all the sequences.
     */
    public List<Sequence> getSequenceList()
    {
        return Collections.unmodifiableList(sequenceList);
    }

    /**
     * Gets the sequence at ths specified index. Equivalent to calling getSequenceList().get(index).
     *
     * @param index
     *         The sequence to be retrieved.
     *
     * @return The sequence at the given index.
     */
    public Sequence getSequence(int index)
    {
        return sequenceList.get(index);
    }

    /**
     * @return The constraints that went into this payload list.
     */
    public Constraint getConstraints()
    {
        return constraints;
    }
}
