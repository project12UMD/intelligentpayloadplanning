package DataHandlers.PayloadList;

/**
 * Represents an action that can be taken.
 */
public abstract class DeploymentAction
{
    private final int NOT_SET = -1;
    private int startTime = NOT_SET;
    private int actionTime;
    private int tube;

    /**
     * Creates a new DeploymentAction.
     *
     * @param tube
     *         The tube this action will take place on.
     * @param actionTime
     *         The amount of time it takes for this action to be performed.
     */
    DeploymentAction(int tube, int actionTime)
    {
        this.tube = tube;
        this.actionTime = actionTime;
    }

    /**
     * @return The tube this action will be performed on.
     */
    public int getTube()
    {
        return tube;
    }

    /**
     * @return The time the action will start.
     *
     * @throws IllegalStateException
     *         The start time has not been set yet.
     */
    public int getStartTime()
    {
        if(startTime == NOT_SET)
        {
            throw new IllegalStateException("Start time not set.");
        }
        return startTime;
    }

    /**
     * Sets the time of this action. Can only be called by methods inside the package to prevent modification.
     *
     * @param startTime
     *         The start time.
     *
     * @throws IllegalArgumentException
     *         The start time must not be less than zero.
     */
    void setStartTime(int startTime)
    {
        if(startTime < 0)
        {
            throw new IllegalArgumentException("Start time cannot be less than zero.");
        }
        this.startTime = startTime;
    }

    /**
     * @return The time it takes for the action to be performed.
     */
    public int getActionTime()
    {
        return actionTime;
    }
}
