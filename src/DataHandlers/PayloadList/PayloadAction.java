package DataHandlers.PayloadList;

import DataHandlers.Constraint.Location;

/**
 * Represents the action of launching a payload.
 */
public class PayloadAction extends DeploymentAction
{
    private int cell;
    private Location target;

    /**
     * Creates a new PayloadAction.
     *
     * @param tube
     *         The tube the payload is launched from.
     * @param actionTime
     *         The time it takes to launch the payload.
     * @param cell
     *         The deploy that the payload is launched from.
     * @param target
     *         The destination of this payload action.
     */
    PayloadAction(int tube, int actionTime, int cell, Location target)
    {
        super(tube, actionTime);
        this.cell = cell;
        this.target = target;
    }

    /**
     * @return The deploy that the payload is launched from.
     */
    public int getCell()
    {
        return cell;
    }

    /**
     * @return The time it takes for the payload to arrive at the target.
     */
    public Location getTarget()
    {
        return target;
    }

    /**
     * Gets the status of this action at the given time.
     *
     * @param time
     *         The current time.
     *
     * @return The status of this action.
     */
    public STATUS getStatus(int time)
    {
        if(time < getStartTime())
        {
            return STATUS.WILL_LAUNCH;
        }
        else if(time < getStartTime() + getActionTime())
        {
            return STATUS.LAUNCHING;
        }
        else if(time < getArrivalTime())
        {
            return STATUS.IN_TRANSIT;
        }
        else
        {
            return STATUS.ARRIVED;
        }
    }

    /**
     * @return The time that the payload will reach its target.
     */
    int getArrivalTime()
    {
        return getStartTime() + getActionTime() + getTarget().getTravelTime();
    }

    /**
     * Represents the status of this action.
     */
    public enum STATUS
    {
        WILL_LAUNCH, LAUNCHING, IN_TRANSIT, ARRIVED
    }
}
