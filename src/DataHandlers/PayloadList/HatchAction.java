package DataHandlers.PayloadList;

/**
 * Represents an action of opening or closing a hatch.
 */
public class HatchAction extends DeploymentAction
{
    private TYPE actionType;

    /**
     * Creates a new HatchAction.
     *
     * @param tube
     *         The tube that the hatch will be opened/closed on.
     * @param actionTime
     *         The time it takes to open or close the hatch.
     * @param actionType
     *         The type of hatch action, either open or close.
     */
    HatchAction(int tube, int actionTime, TYPE actionType)
    {
        super(tube, actionTime);
        this.actionType = actionType;
    }

    /**
     * @return The type of hatch action, either open or close.
     */
    public TYPE getActionType()
    {
        return actionType;
    }

    /**
     * Gets the current status of this action at the given time.
     *
     * @param time
     *         The current time.
     *
     * @return The status of this action.
     */
    public STATUS getStatus(int time)
    {
        if(getActionType() == TYPE.OPEN)
        {
            return openStatus(time);
        }
        else
        {
            return closeStatus(time);
        }
    }

    /**
     * Gets the status of a open tube action.
     *
     * @param time
     *         The current time.
     *
     * @return The status of this action.
     */
    private STATUS openStatus(int time)
    {
        if(time < getStartTime())
        {
            return STATUS.WILL_OPEN;
        }
        else if(time < getStartTime() + getActionTime())
        {
            return STATUS.OPENING;
        }
        else
        {
            return STATUS.OPEN;
        }
    }

    /**
     * Gets the status of a close tube action.
     *
     * @param time
     *         The current time.
     *
     * @return The status of this action.
     */
    private STATUS closeStatus(int time)
    {
        if(time < getStartTime())
        {
            return STATUS.WILL_CLOSE;
        }
        else if(time < getStartTime() + getActionTime())
        {
            return STATUS.CLOSING;
        }
        else
        {
            return STATUS.CLOSED;
        }
    }

    /**
     * An enum that represents the status of a hatch action.
     */
    public enum STATUS
    {
        WILL_OPEN, OPENING, OPEN, WILL_CLOSE, CLOSING, CLOSED
    }

    /**
     * An enum that represents the type of hatch action.
     */
    public enum TYPE
    {
        OPEN, CLOSE
    }
}
