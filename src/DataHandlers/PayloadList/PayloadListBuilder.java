package DataHandlers.PayloadList;

import DataHandlers.Constraint.Constraint;
import DataHandlers.Constraint.Location;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A builder that is used to create immutable PayloadLists.
 */
public class PayloadListBuilder
{
    private Constraint constraints;
    private List<Sequence> sequenceList;
    private Integer currentSequence = null;
    private Integer currentTube = null;
    private Boolean[][] cellsLaunched;
    private Boolean[] tubesOpened;
    private boolean tubeSwitch = false;

    /**
     * Creates a new PayloadList builder.
     *
     * @param constraints
     *         The constraints that will go into the new PayloadList
     */
    public PayloadListBuilder(Constraint constraints)
    {
        this.constraints = constraints;
        sequenceList = new ArrayList<>();
        for(int i = 0; i < constraints.getHatchLimit(); i++)
        {
            sequenceList.add(new Sequence(constraints));
        }
        cellsLaunched = new Boolean[constraints.getTubeAmount()][constraints.getCellAmount()];
        tubesOpened = new Boolean[constraints.getTubeAmount()];
        for(int i = 0; i < constraints.getTubeAmount(); i++)
        {
            Arrays.fill(cellsLaunched[i], false);
        }
        Arrays.fill(tubesOpened, false);
    }

    /**
     * Sets the sequence to add deployments too.
     *
     * @param index
     *         The sequence number to add deployments too.
     *
     * @return this.
     *
     * @throws IllegalArgumentException
     *         When the index is less than zero and greater than the hatch limit.
     */
    public PayloadListBuilder sequence(int index)
    {
        if(index < 0 || index >= constraints.getHatchLimit())
        {
            throw new IllegalArgumentException("Index must be greater than zero and less then the hatch limit.");
        }

        if(currentTube != null && !tubeSwitch)
        {
            sequenceList.get(currentSequence)
                        .addAction(new HatchAction(currentTube, constraints.getHatchTime(), HatchAction.TYPE.CLOSE));
        }

        currentSequence = index;
        currentTube = null;
        tubeSwitch = false;
        return this;
    }

    /**
     * Deploys a payload from the given cell, set sequence and tube, to the given target.
     *
     * @param cell
     *         The cell that the payload should be fired from.
     * @param target
     *         The target of this deployment.
     *
     * @return this.
     *
     * @throws IllegalArgumentException
     *         When cell is less than zero or greater than the cell amount.
     * @throws IllegalStateException
     *         When Sequence has not been set.
     * @throws IllegalStateException
     *         When Tube has not been set.
     */
    public PayloadListBuilder deploy(int cell, Location target)
    {
        if(currentSequence == null)
        {
            throw new IllegalStateException("Sequence must be set first.");
        }
        if(currentTube == null)
        {
            throw new IllegalStateException("Tube must be set first.");
        }
        if(cell < 0 || cell >= constraints.getCellAmount())
        {
            throw new IllegalArgumentException("Cell must be greater than zero and less than the cell amount.");
        }
        if(cellsLaunched[currentTube][cell] || !constraints.getCellStatus(currentTube, cell))
        {
            throw new IllegalArgumentException("Cell not available. (Cell: " + cell + " Tube: " + currentTube + ")");
        }

        if(tubeSwitch)
        {
            sequenceList.get(currentSequence)
                        .addAction(new HatchAction(currentTube, constraints.getHatchTime(),
                                                   HatchAction.TYPE.OPEN));
            tubeSwitch = false;
        }
        sequenceList.get(currentSequence)
                    .addAction(new PayloadAction(currentTube, constraints.getDeploymentTime(), cell, target));
        cellsLaunched[currentTube][cell] = true;
        return this;
    }

    /**
     * Sets the tube from which future deployments will deploy from.
     *
     * @param tube
     *         The tube for further deployments.
     *
     * @return this.
     *
     * @throws IllegalArgumentException
     *         When tube is less than zero or greater than the tube amount.
     * @throws IllegalArgumentException
     *         When this tube has been opened before on this or different sequence.
     * @throws IllegalStateException
     *         When sequence is not set.
     */
    public PayloadListBuilder tube(int tube)
    {
        if(currentSequence == null)
        {
            throw new IllegalStateException("Sequence must be set first.");
        }
        if(tube < 0 || tube >= constraints.getTubeAmount())
        {
            throw new IllegalArgumentException("tube must be greater than zero and less than the tube amount.");
        }
        if(tubesOpened[tube])
        {
            throw new IllegalArgumentException("Tube already used.");
        }

        if(currentTube != null && !tubeSwitch)
        {
            sequenceList.get(currentSequence)
                        .addAction(new HatchAction(currentTube, constraints.getHatchTime(), HatchAction.TYPE.CLOSE));
        }
        currentTube = tube;
        tubesOpened[tube] = true;
        tubeSwitch = true;
        return this;
    }

    /**
     * Builds a new PayloadList.
     *
     * @return A new immutable PayloadList with the given settings set.
     */
    public PayloadList build()
    {
        if(currentSequence != null && currentTube != null)
        {
            sequenceList.get(currentSequence)
                        .addAction(new HatchAction(currentTube, constraints.getHatchTime(), HatchAction.TYPE.CLOSE));
        }
        return new PayloadList(this);
    }

    /**
     * Gets the sequence list. Used by the PayloadList constructor to create the payload list.
     *
     * @return The sequence list of this builder.
     */
    List<Sequence> getSequenceList()
    {
        return sequenceList;
    }

    /**
     * Gets the constraints for this builder. Used by the PayloadList constructor to create the PayloadList
     *
     * @return The constraints that went into this builder.
     */
    Constraint getConstraints()
    {
        return constraints;
    }

    /**
     * Gets the statuses of the cells. Used by the PayloadList constructor to create the PayloadList.
     *
     * @return The status of all the cells.
     */
    Boolean[][] getCellsLaunched()
    {
        return cellsLaunched;
    }
}
