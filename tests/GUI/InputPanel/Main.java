package GUI.InputPanel;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


public class Main extends Application
{
    private BorderPane root;
    private InputPanel input;

    public static void main(String[] args)
    {
        launch(args);
    }

    /**
     * @param primaryStage
     *
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        primaryStage.setTitle("Intelligent Payload Planning Tool");
        primaryStage.setScene(new Scene(root, 500, 500));
        primaryStage.setMinWidth(800);
        primaryStage.setMinHeight(740);
        primaryStage.show();
    }
}
