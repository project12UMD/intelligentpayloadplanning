package Optimizer.BranchBoundOptimizer;

import DataHandlers.Constraint.Constraint;
import DataHandlers.PayloadList.PayloadList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * Created by brian on 2/3/18.
 */
public class SequenceBinTest
{
    private static final int TUBES = 4;
    private static final int CELLS = 6;
    private static final int TUBES_SIM = 2;
    private static final int CELLS_SIM = 2;
    private static final int TUBE_TIME = 2;
    private static final int CELL_TIME = 2;
    private static final int LOCATIONS = 2;
    private static final int LOC_PAYLOADS = 2;
    private static final int LOC_TIME = 2;
    private static final int GOAL = LOCATIONS * LOC_PAYLOADS;
    private static final int GROUPS = CELLS / CELLS_SIM;
    Constraint constraints;

    /**
     *
     */
    @BeforeEach
    public void setup()
    {
        Constraint.Builder builder = new Constraint.Builder();
        builder.setTubeAmount(TUBES)
               .setCellAmount(CELLS)
               .setHatchLimit(TUBES_SIM)
               .setDeploymentLimit(CELLS_SIM)
               .setHatchTime(TUBE_TIME)
               .setDeploymentTime(CELL_TIME);
        for(int i = 0; i < LOCATIONS; i++)
        {
            builder.addLocation(LOC_PAYLOADS, LOC_TIME);
        }
        constraints = builder.build();
    }

    /**
     * ensures constructor
     */
    @Test
    public void testConstructor()
    {
        SequenceBin testBin = new SequenceBin(constraints);
        assertNotNull(testBin);
    }

    /**
     *
     */
    @Test
    public void testGetMin()
    {
        SequenceBin testBin = new SequenceBin(constraints);
        Method method = null;
        try {
            method = testBin.getClass().getDeclaredMethod("getMin", null);
            method.setAccessible(true);
            try {
                SequenceBin.Sequence result = (SequenceBin.Sequence)method.invoke(testBin, null);
                assertEquals(0, result.value());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

    }

    /**
     *
     */
    @Test
    public void testGetPayloadList()
    {
        SequenceBin testBin = new SequenceBin(constraints);
        Node currentNode = new Node(constraints);
        testBin.add(currentNode);
        for(int i = 0; i < TUBES - 1; i++)
        {
            for(int j = 0; j < GROUPS - 1; j++)
            {
                currentNode = currentNode.nextGroup();
                testBin.add(currentNode);
            }
            currentNode = currentNode.nextTube();
            testBin.add(currentNode);
        }
        PayloadList output = testBin.getPayloadList();
        System.out.println("Test");
    }

    /**
     *
     */
    @Test
    public void testGetValue()
    {
        SequenceBin testBin = new SequenceBin(constraints);
        if(testBin.getValue() < 0){
            fail();
        }
    }

    /**
     *
     */
    @Test
    public void testRemove()
    {
        SequenceBin testBin = new SequenceBin(constraints);
        Node currentNode = new Node(constraints);
        testBin.add(currentNode);
        testBin.remove(currentNode);

    }
}
