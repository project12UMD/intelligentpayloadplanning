package Optimizer.BranchBoundOptimizer;

import DataHandlers.Constraint.Constraint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests for the Node class.
 */
public class NodeTest
{
    private static final int TUBES = 4;
    private static final int CELLS = 6;
    private static final int TUBES_SIM = 2;
    private static final int CELLS_SIM = 2;
    private static final int TUBE_TIME = 2;
    private static final int CELL_TIME = 2;
    private static final int LOCATIONS = 2;
    private static final int LOC_PAYLOADS = 2;
    private static final int LOC_TIME = 2;
    private static final int GOAL = LOCATIONS * LOC_PAYLOADS;
    private Constraint constraints;

    /**
     *
     */
    @BeforeEach
    public void setup()
    {
        Constraint.Builder builder = new Constraint.Builder();
        builder.setTubeAmount(TUBES)
               .setCellAmount(CELLS)
               .setHatchLimit(TUBES_SIM)
               .setDeploymentLimit(CELLS_SIM)
               .setHatchTime(TUBE_TIME)
               .setDeploymentTime(CELL_TIME);
        for(int i = 0; i < LOCATIONS; i++)
        {
            builder.addLocation(LOC_PAYLOADS, LOC_TIME);
        }
        constraints = builder.build();
    }

    /**
     *
     */
    @Test
    public void testRootConstructor()
    {
        final int ROOT_TUBE = 0;
        final int ROOT_GROUP = 0;

        Node testGroup = new Node(constraints);

        assertEquals("Launched Cells Test", CELLS_SIM, testGroup.getLaunchedCells());
        assertEquals("Remaining Cells Test", CELLS * TUBES - CELLS_SIM, testGroup.getRemainingCells());
        assertEquals("Value Test", CELL_TIME, testGroup.getValue());
        assertEquals("Prev Node Test", null, testGroup.getPrevNode());
    }

    /**
     *
     */
    @Test
    public void testNextTube()
    {
        Node prevGroup = new Node(constraints);
        Node testGroup = prevGroup.nextTube();

        assertEquals("Launched Cells Test", CELLS_SIM * 2, testGroup.getLaunchedCells());
        assertEquals("Remaining Cells Test", CELLS * TUBES - (CELLS_SIM + CELLS), testGroup.getRemainingCells());
        assertEquals("Value Test", CELL_TIME + TUBE_TIME, testGroup.getValue());
        assertEquals("Prev Node Test", prevGroup, testGroup.getPrevNode());
    }

    /**
     *
     */
    @Test
    public void testNextGroup()
    {
        Node prevGroup = new Node(constraints);
        Node testGroup = prevGroup.nextGroup();

        assertEquals("Launched Cells Test", CELLS_SIM * 2, testGroup.getLaunchedCells());
        assertEquals("Remaining Cells Test", CELLS * TUBES - CELLS_SIM * 2, testGroup.getRemainingCells());
        assertEquals("Value Test", CELL_TIME, testGroup.getValue());
        assertEquals("Prev Node Test", prevGroup, testGroup.getPrevNode());
    }
}
