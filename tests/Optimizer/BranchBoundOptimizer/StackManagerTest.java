package Optimizer.BranchBoundOptimizer;

import DataHandlers.Constraint.Constraint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Tests for the Stack Manager.
 */
public class StackManagerTest
{
    private static final int TUBES = 4;
    private static final int CELLS = 6;
    private static final int TUBES_SIM = 2;
    private static final int CELLS_SIM = 2;
    private static final int TUBE_TIME = 2;
    private static final int CELL_TIME = 2;
    private static final int LOCATIONS = 7;
    private static final int LOC_PAYLOADS = 2;
    private static final int LOC_TIME = 2;
    private static final int GOAL = LOCATIONS * LOC_PAYLOADS;
    private static final int GROUPS = CELLS / CELLS_SIM;
    private static final int TOTAL_PAYLOADS = LOC_PAYLOADS * LOCATIONS;
    private Constraint constraints;

    /**
     * Checks to see if the current node on the stack is what is expected.
     *
     * @param stack
     *         The stack manager of the current node.
     * @param tubeId
     *         The id of the expected tube.
     * @param cells
     *         The expected amount of cells at the node.
     *
     * @throws NoSuchFieldException
     *         If there is an error finding the field.
     * @throws IllegalAccessException
     *         If the access manager is on.
     */
    private static void testCurrentNode(StackManager stack, int tubeId, int cells) throws NoSuchFieldException,
                                                                                          IllegalAccessException
    {
        Field nodeField = StackManager.class.getDeclaredField("currentNode");
        nodeField.setAccessible(true);
        Node currentNode = (Node) nodeField.get(stack);
        assertEquals(tubeId, currentNode.getTube()
                                        .getId(), "Tube Test");
        assertEquals(cells, currentNode.getTube()
                                       .getAvailableCells(), "Cells Test");
    }

    /**
     *
     */
    @BeforeEach
    public void setup()
    {
        Constraint.Builder builder = new Constraint.Builder();
        builder.setTubeAmount(TUBES)
               .setCellAmount(CELLS)
               .setHatchLimit(TUBES_SIM)
               .setDeploymentLimit(CELLS_SIM)
               .setHatchTime(TUBE_TIME)
               .setDeploymentTime(CELL_TIME);
        for(int i = 0; i < LOCATIONS; i++)
        {
            builder.addLocation(LOC_PAYLOADS, LOC_TIME);
        }
        constraints = builder.build();
    }

    /**
     * @throws NoSuchFieldException
     *         If there is an error finding the field.
     * @throws IllegalAccessException
     *         If the access manager is on.
     */
    @Test
    public void testConstructor() throws NoSuchFieldException, IllegalAccessException
    {
        final int ROOT_TUBE = 0;

        StackManager testStack = new StackManager(constraints);
        testCurrentNode(testStack, ROOT_TUBE, CELLS);
    }

    /**
     * @throws NoSuchFieldException
     *         If there is an error finding the field.
     * @throws IllegalAccessException
     *         If the access manager is on.
     */
    @Test
    public void testNextTubeTransition() throws NoSuchFieldException, IllegalAccessException
    {
        final int TUBE = 1;
        final int GROUP = 0;

        StackManager testStack = new StackManager(constraints);
        assertEquals(true, testStack.nextTube(), "Transition Test");
        testCurrentNode(testStack, TUBE, GROUP);
    }

    /**
     * @throws NoSuchFieldException
     *         If there is an error finding the field.
     * @throws IllegalAccessException
     *         If the access manager is on.
     */
    @Test
    public void testNextTubeNoTube() throws NoSuchFieldException, IllegalAccessException
    {
        final int TUBE = TUBES - 1;
        final int GROUP = 0;

        StackManager testStack = new StackManager(constraints);

        for(int i = 0; i < TUBES - 1; i++)
        {
            assertEquals(true, testStack.nextTube(), "Transition Test");
        }
        assertEquals(false, testStack.nextTube(), "No Tube Test");
        testCurrentNode(testStack, TUBE, GROUP);
    }

    /**
     * @throws NoSuchFieldException
     *         If there is an error finding the field.
     * @throws IllegalAccessException
     *         If the access manager is on.
     */
    @Test
    public void testNextGroupTransition() throws NoSuchFieldException, IllegalAccessException
    {
        final int TUBE = 0;
        final int GROUP = 1;

        StackManager testStack = new StackManager(constraints);
        assertEquals(true, testStack.nextGroup(), "Transition Test");
        testCurrentNode(testStack, TUBE, GROUP);
    }

    /**
     * Tests the next group thing.
     *
     * @throws NoSuchFieldException
     *         If there is an error finding the field.
     * @throws IllegalAccessException
     *         If the access manager is on.
     */
    @Test
    public void testNextGroupNoGroup() throws NoSuchFieldException, IllegalAccessException
    {
        final int TUBE = 0;
        final int GROUP = GROUPS - 1;

        StackManager testStack = new StackManager(constraints);

        for(int i = 0; i < GROUPS - 1; i++)
        {
            assertEquals(true, testStack.nextGroup(), "Transition Test");
        }
        assertEquals(false, testStack.nextGroup(), "No Group Test");
        testCurrentNode(testStack, TUBE, GROUP);
    }

    /**
     * Tests the remove method
     *
     * @throws NoSuchFieldException
     *         If there is an error finding the field.
     * @throws IllegalAccessException
     *         If the access manager is on.
     */
    @Test
    public void testRemove() throws NoSuchFieldException, IllegalAccessException
    {
        final int TUBE = 0;
        final int GROUP = 0;

        StackManager testStack = new StackManager(constraints);
        testStack.nextGroup();
        testStack.remove();
        testCurrentNode(testStack, TUBE, GROUP);
    }

    /**
     * Tests the continueSearch() method and ensures it returns false on the condition that there are not enough
     * remaining cells.
     */
    @Test
    public void testContinueSearchNoRemainingCells()
    {
        StackManager testStack = new StackManager(constraints);
        for(int i = 0; i < TUBES - 1; i++)
        {
            testStack.nextTube();
        }
        assertEquals(false, testStack.continueSearch());
    }

    /**
     * Tests the continueSearch() method and ensures it returns false on the condition that the current value exceeds
     * the min value.
     */
    @Test
    public void testContinueSearchMinValue()
    {
        StackManager testStack = new StackManager(constraints);

        for(int i = 0; i < TOTAL_PAYLOADS / CELLS; i++)
        {
            for(int j = 0; j < GROUPS - 1; j++)
            {
                testStack.nextGroup();
            }
            testStack.nextTube();
        }
        try
        {
            testStack.getMinList();
        }
        catch(IllegalStateException e)
        {
            fail("Min List not found");
        }
        for(int i = 0; i < GROUPS; i++)
        {
            testStack.remove();
        }
        testStack.nextTube();
        testStack.nextGroup();
        assertEquals(false, testStack.continueSearch());
    }

    /**
     * Tests the ContinueSearch() method and ensures that it returns false on the condition there are enough cells.
     */
    @Test
    public void testContinueSearchEnoughCells()
    {
        StackManager testStack = new StackManager(constraints);

        for(int i = 0; i < TOTAL_PAYLOADS / CELLS; i++)
        {
            for(int j = 0; j < GROUPS - 1; j++)
            {
                testStack.nextGroup();
            }
            testStack.nextTube();
        }
        testStack.nextGroup();
        assertEquals(false, testStack.continueSearch());
    }

    /**
     * Tests to make sure that the ContinueSearch() method returns true.
     */
    @Test
    public void testContinueSearchTrue()
    {
        StackManager testStack = new StackManager(constraints);
        assertEquals(true, testStack.continueSearch());
    }


}
