package Optimizer.BranchBoundOptimizer;

import DataHandlers.Constraint.CellStatuses;
import DataHandlers.Constraint.Constraint;
import DataHandlers.PayloadList.PayloadList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for the Branch Bound Optimizer.
 */
public class BranchBoundOptimizerTest
{
    private Constraint.Builder builder;
    private CellStatuses statuses;
    private int bestActionTime = 0;
    private int bestArrivalTime = 0;

    /**
     * Sets default settings for all the constraints for each test.
     */
    @BeforeEach
    public void setup()
    {
        builder = new Constraint.Builder();
        builder.setTubeAmount(4)
               .setCellAmount(4)
               .setDeploymentLimit(2)
               .setHatchLimit(2)
               .setHatchTime(4)
               .setDeploymentTime(3);
        statuses = null;
    }

    /**
     * Applys the standard test after the input has been adjusted.
     */
    @AfterEach
    public void postTest()
    {
        PayloadList result;
        if(statuses == null)
        {
            result = BranchBoundOptimizer.optimize(builder.build());
        }
        else
        {
            result = BranchBoundOptimizer.optimize(builder.build(statuses));
        }
        assertEquals(bestActionTime, result.getActionTime(), "Action time not optimal.");
        assertEquals(bestArrivalTime, result.getActionTime(), "Arrival time not optimal.");
    }

    /**
     * Tests the default settings of the builder.
     */
    @Test
    public void testDefault()
    {
        builder.addLocation(4, 3);
        bestActionTime = 11;
        bestArrivalTime = 11;
    }

    /**
     * This ensures that the optimizer can handle when the sim tubes are equal to the total amount of tubes.
     */
    @Test
    public void testSimTubes()
    {
        bestActionTime = 11;
        bestArrivalTime = 11;
        builder.setTubeAmount(4)
               .setHatchLimit(4)
               .addLocation(4, 3);
    }

    /**
     * This test ensures that the optimizer can handle large inputs.
     */
    @Test
    public void testLargeInput()
    {
        bestActionTime = 96;
        bestArrivalTime = 96;
        builder.setTubeAmount(15)
               .setCellAmount(10)
               .setDeploymentLimit(1)
               .setHatchLimit(1)
               .addLocation(4, 3)
               .addLocation(20, 1);
    }

    /**
     * This test ensures that the optimizer handles unavailable cells correctly.
     */
    @Test
    public void testFirstTubeUnavailable()
    {
        bestActionTime = 11;
        bestArrivalTime = 11;
        builder.setTubeAmount(3)
               .setCellAmount(3)
               .addLocation(4, 3);
        statuses = new CellStatuses(3, 3);
        statuses.setStatus(0, 0, false);
        statuses.setStatus(0, 1, false);
        statuses.setStatus(0, 2, false);
    }

    /**
     * This test ensures that the optimizer handles unavailable cells correctly.
     */
    @Test
    public void testSecondTubeUnavailable()
    {
        bestActionTime = 11;
        bestArrivalTime = 11;
        builder.setTubeAmount(3)
               .setCellAmount(3)
               .addLocation(4, 3);
        statuses = new CellStatuses(3, 3);
        statuses.setStatus(1, 0, false);
        statuses.setStatus(1, 1, false);
        statuses.setStatus(1, 2, false);
    }

    /**
     * Test case that threw a min list not found error.
     */
    @Test
    public void minListNotFoundError()
    {
        bestActionTime = 39;
        bestArrivalTime = 39;
        builder.setTubeAmount(5)
               .setCellAmount(3)
               .setHatchLimit(1)
               .setDeploymentLimit(1)
               .setHatchTime(3)
               .setDeploymentTime(3)
               .addLocation(4, 3)
               .addLocation(3, 3);
        statuses = new CellStatuses(5, 3);
        statuses.setStatus(0, 1, false);
    }

    /**
     * Test case that threw a no such element exception.
     */
    @Test
    public void NoSuchElementErrorOne()
    {
        bestActionTime = 11;
        bestArrivalTime = 11;
        builder.setTubeAmount(3)
               .setCellAmount(3)
               .setDeploymentLimit(2)
               .setHatchLimit(2)
               .addLocation(4, 3);
    }

    /**
     * Test case that threw a no such element exception.
     */
    @Test
    public void NoSuchElementErrorTwo()
    {
        bestActionTime = 14;
        bestArrivalTime = 14;
        builder.setTubeAmount(7)
               .setCellAmount(4)
               .setDeploymentLimit(2)
               .setHatchLimit(3)
               .addLocation(7, 3);
    }
}
