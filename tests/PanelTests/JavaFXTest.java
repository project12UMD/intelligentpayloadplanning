package PanelTests;

import org.junit.Rule;

/**
 * class to extend your test class to run JavaFX thread to make JavaFX testing easier
 */
public abstract class JavaFXTest
{
    @Rule
    public JavaFXThreadingRule jfxRule = new JavaFXThreadingRule();
}
