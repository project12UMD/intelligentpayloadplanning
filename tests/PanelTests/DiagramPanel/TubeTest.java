package PanelTests.DiagramPanel;

import GUI.DiagramPanel.Tube;
import javafx.embed.swing.JFXPanel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for the Tube Class
 */
class TubeTest{

    final double INITSIZE = 200;
    final int TESTCELL = 5;
    JFXPanel stub = new JFXPanel();
    Tube test = new Tube(INITSIZE);

    /**
     * Tests initial Values of each cell
     */
    @Test
    void testInitialValues(){
        test.drawTube(TESTCELL);
        assertTrue(test.canEdit);
        for(int cell = 0; cell< TESTCELL; cell++){
        assertFalse(test.getFaultStatus(cell));
        assertFalse(test.getWaitingStatus(cell));
        }

    }

    /**
     * Tests waiting status of cell
     */
    @Test
    void getWaitingStatusTest() {
        test.drawTube(TESTCELL);
        for(int i = 0; i < test.getCellCount(); i++)
        {
            assertFalse(test.getWaitingStatus(i));
        }
        for(int i = 0; i < test.getCellCount(); i++)
        {
            test.prepareCell(i);
            assertTrue(test.getWaitingStatus(i));
        }

    }

    /**
     * Tests tube drawn with the right amount of cells
     */
    @Test
    void drawTubeTest() {
        test.drawTube(TESTCELL);
        assertEquals(test.getCellCount(), TESTCELL);

    }

    /**
     * Tests getting cells
     */
    @Test
    void getCellCountTest() {
        test.drawTube(TESTCELL);
        assertEquals(test.getCellCount(), TESTCELL);

    }

    /**
     * Tests the cell status when fired
     */
    @Test
    void fireCellTest() {
        test.drawTube(TESTCELL);
        test.fireCell(0);
        assertFalse(test.getWaitingStatus(0));

    }

    /**
     * Tests cell status when it is waiting for launch
     */
    @Test
    void prepareCellTest() {
        test.drawTube(TESTCELL);
        test.prepareCell(0);
        assertTrue(test.getWaitingStatus(0));

    }

    /**
     * Tests cell status when it reaches destination
     */
    @Test
    void finishCellTest() {
        test.drawTube(TESTCELL);
        test.finishCell(0);
        assertFalse(test.getWaitingStatus(0));

    }

    /**
     * Tests cell status when cell is set to faulted
     */
    @Test
    void getFaultStatusTest() {
        test.drawTube(TESTCELL);
        test.faultCell(0);
        assertTrue(test.getFaultStatus(0));

    }

    /**
     * Tests canvas when screen is resized
     */
    @Test
    void redrawTest() {
        test.redraw(INITSIZE*2);
        assertEquals(test.getWidth(), INITSIZE*2);

    }

}