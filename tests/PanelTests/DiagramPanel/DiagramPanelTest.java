package PanelTests.DiagramPanel;

import DataHandlers.Constraint.Constraint;
import DataHandlers.PayloadList.PayloadList;
import GUI.DiagramPanel.DiagramPanel;
import Optimizer.BranchBoundOptimizer.BranchBoundOptimizer;
import Optimizer.OptimizationFunction;
import javafx.embed.swing.JFXPanel;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


/***
 *
 * the test class for the diagram panel
 *
 * Covers: Tube and cell creation/removal
 *
 */

class DiagramPanelTest
{
    JFXPanel panel = new JFXPanel();
    DiagramPanel tubeTest = new DiagramPanel();

    /***
     * tests the making of tubes and the value of their tube number
     */
    @Test
    void testTubeMaking()
    {
        for(int tube = 1; tube <= 20 ; tube++){
            tubeTest.update(tube, 5);
            assertEquals(tubeTest.topRow.getChildren().size() + tubeTest.bottomRow.getChildren().size(),tube);
        }

    }

    /***
     * tests the removal of tubes
     */
    @Test
    void testTubeRemoving()
    {
        for(int tube = 20; tube >= 1 ; tube--){
            tubeTest.update(tube,3);
            assertEquals(tubeTest.topRow.getChildren().size() + tubeTest.bottomRow.getChildren().size(),tube);
        }

    }

    /***
     * tests the making of cells
     */
    @Test
    void testCellMaking()
    {
        for(int cell = 1; cell <= 10 ; cell++){
            tubeTest.update(1,cell);
            assertEquals(tubeTest.tubes.get(0).getCellCount(),cell);
        }

    }

    /***
     * tests the removal of cells
     */
    @Test
    void testCellRemoving()
    {
        for(int cell = 10; cell >= 1 ; cell--){
            tubeTest.update(1,cell);
            assertEquals(tubeTest.tubes.get(0).getCellCount(),cell);
        }

    }

    /***
     *
     * tests the defaults to make sure that a cell is available when created
     */
    @Test
    void testCellDefaults()
    {

        for(int cells = 1; cells <=10; cells++){
            tubeTest.update(1,cells);
            for(int cell = 0 ; cell < cells ; cell++){
            assertFalse(tubeTest.tubes.get(0).getFaultStatus(cell));
            assertFalse(tubeTest.tubes.get(0).getWaitingStatus(cell));
            }
        }


    }

    /***
     * ensures update(payload) is displaying the amount of tubes made
     */
    @Test
    void testUpdatePayload(){
        final int TUBES = 5, CELLS = 5, SIMUL = 1, TIME = 3;
        Constraint limits = new Constraint.Builder().setTubeAmount(TUBES)
                .setCellAmount(CELLS)
                .setHatchLimit(SIMUL)
                .setDeploymentLimit(SIMUL)
                .setHatchTime(TIME)
                .setDeploymentTime(TIME)
                .addLocation(CELLS, TIME)
                .build();
        OptimizationFunction optimizer = BranchBoundOptimizer::optimize;
        PayloadList payload = optimizer.apply(limits);

        tubeTest.update(payload);

        assertEquals(tubeTest.topRow.getChildren().size() + tubeTest.bottomRow.getChildren().size(),TUBES);
        for(int tube = 0; tube < TUBES; tube++){
            for(int cell = 0; cell < CELLS; cell++){
                assertEquals(tubeTest.tubes.get(tube).getCellCount(),CELLS);
            }
        }
    }

    /**
     * Ensures update(payload, time) runs through tubes
     */
    @Test
    void testUpdatePayloadTime(){
        final int TUBES = 5, CELLS = 5, SIMUL = 1, TIME = 3;
        Constraint limits = new Constraint.Builder().setTubeAmount(TUBES)
                .setCellAmount(CELLS)
                .setHatchLimit(SIMUL)
                .setDeploymentLimit(SIMUL)
                .setHatchTime(TIME)
                .setDeploymentTime(TIME)
                .addLocation(CELLS, TIME)
                .build();
        OptimizationFunction optimizer = BranchBoundOptimizer::optimize;
        PayloadList payload = optimizer.apply(limits);

        tubeTest.update(TUBES, CELLS);

        tubeTest.update(payload, TIME);

        assertEquals(tubeTest.topRow.getChildren().size() + tubeTest.bottomRow.getChildren().size(),TUBES);

        for(int tube = 0; tube < TUBES; tube++){
            for(int cell = 0; cell < CELLS; cell++){
                assertEquals(tubeTest.tubes.get(tube).getCellCount(),CELLS);
            }
        }
    }
}