package PanelTests.InputPanel;

import DataHandlers.Constraint.Constraint;
import GUI.DiagramPanel.DiagramPanel;
import GUI.InputPanel.InputPanel;
import javafx.embed.swing.JFXPanel;
import javafx.scene.layout.VBox;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.*;

/***
 * draft input panel test
 */

class InputPanelTest {

    //make reflection methods

    JFXPanel thread = new JFXPanel();


    /**
     * ensures that a location is added and then removed
     *
     *
     * @throws NoSuchMethodException this is because the method is private and reflection is needed.
     */
    @Test
    void addRemoveLocationTest() throws NoSuchMethodException {
        DiagramPanel stub = new DiagramPanel();
        InputPanel test = new InputPanel(stub::update, stub::fetchCellStatuses);

        final int ADD_OUTCOME = 2;
        Method testMethod = test.getClass().getDeclaredMethod("addLocation", null);
        assertFalse(testMethod.equals(null));
        try {
            testMethod.setAccessible(true);
            testMethod.invoke(test, null);

            Constraint build = test.getConstraints();
            assertEquals(ADD_OUTCOME,build.getLocations().size() );
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        final int REMOVE_OUTCOME = 1;
        testMethod = test.getClass().getDeclaredMethod("removeLocation", null);
        assertFalse(testMethod.equals(null));
        try {
            testMethod.setAccessible(true);
            testMethod.invoke(test, null);
            Constraint build = test.getConstraints();
            assertEquals(REMOVE_OUTCOME,build.getLocations().size() );

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * ensures that function returns a constraints class and is not null
     */
    @Test
    void getConstraintsTest() {
        DiagramPanel stub2 = new DiagramPanel();
        InputPanel test2 = new InputPanel(stub2::update, stub2::fetchCellStatuses);
        Constraint constraint = test2.getConstraints();
        assertNotNull(constraint);
        assertEquals(constraint.getClass(), Constraint.class);
    }

    /**
     * ensures that the update function changes the diagram panel
     */
    @Test
    void callUpdateTest(){
        DiagramPanel stub = new DiagramPanel();
        InputPanel test = new InputPanel(stub::update, stub::fetchCellStatuses);

        test.callUpdate();
        assertEquals(stub.topRow.getChildren().size(), 1);
    }

}