package PanelTests.ListPanel;

import DataHandlers.Constraint.Constraint;
import DataHandlers.PayloadList.DeploymentAction;
import DataHandlers.PayloadList.PayloadList;
import GUI.GanttChart.GanttChart;
import GUI.ListPanel.ListPanel;
import Optimizer.BranchBoundOptimizer.BranchBoundOptimizer;
import Optimizer.OptimizationFunction;
import javafx.embed.swing.JFXPanel;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.layout.HBox;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for the List Panel
 */
class ListPanelTest {

    JFXPanel threaad = new JFXPanel();
    /**
     * ensures that the list panel was made
     */
    @Test
    void initComponentsTest() {
        ListPanel test = new ListPanel();
        Field fieldChart = null;
        Field fieldXAXIS = null;
        Field fieldYAXIS = null;
        Field fieldHBox = null;
        try {
            fieldChart = test.getClass().getDeclaredField("chart");
            fieldXAXIS = test.getClass().getDeclaredField("xAxis");
            fieldYAXIS = test.getClass().getDeclaredField("yAxis");
            fieldHBox = test.getClass().getDeclaredField("listHBox");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        assertEquals(fieldChart.getType(), GanttChart.class);
        assertEquals(fieldHBox.getType(), HBox.class);
        assertEquals(fieldXAXIS.getType(), NumberAxis.class);
        assertEquals(fieldYAXIS.getType(), CategoryAxis.class);
    }

    /**
     * ensures that the list panel has the same amount as the payload list - 1
     *
     * @throws IllegalAccessException for converting cardFiled to HBox
     * @throws NoSuchFieldException for getDeclaredField()
     */
    @Test
    void updatePayloadTest() throws IllegalAccessException, NoSuchFieldException {
        ListPanel test = new ListPanel();
        final int TUBES = 5, CELLS = 5, SIMUL = 1, TIME = 3;
        Constraint limits = new Constraint.Builder().setTubeAmount(TUBES)
                .setCellAmount(CELLS)
                .setHatchLimit(SIMUL)
                .setDeploymentLimit(SIMUL)
                .setHatchTime(TIME)
                .setDeploymentTime(TIME)
                .addLocation(CELLS, TIME)
                .build();
        OptimizationFunction optimizer = BranchBoundOptimizer::optimize;
        PayloadList payload = optimizer.apply(limits);

        test.update(payload);

        Field cardField = test.getClass().getDeclaredField("listHBox");
        cardField.setAccessible(true);
        HBox box = (HBox)cardField.get(test);
        assertEquals(payload.getOrderList().size(), box.getChildren().size());

    }

    /**
     * ensures that the list panel has the same amount as the payload list by the end of time
     *
     * @throws IllegalAccessException for converting cardFiled to HBox
     * @throws NoSuchFieldException for getDeclaredField()
     */
    @Test
    void updatePayloadTimeTest() throws NoSuchFieldException, IllegalAccessException {
        ListPanel test = new ListPanel();
        final int TUBES = 5, CELLS = 5, SIMUL = 1, TIME = 3;
        Constraint limits = new Constraint.Builder().setTubeAmount(TUBES)
                .setCellAmount(CELLS)
                .setHatchLimit(SIMUL)
                .setDeploymentLimit(SIMUL)
                .setHatchTime(TIME)
                .setDeploymentTime(TIME)
                .addLocation(CELLS, TIME)
                .build();
        OptimizationFunction optimizer = BranchBoundOptimizer::optimize;
        PayloadList payload = optimizer.apply(limits);

        test.update(payload);
        test.update(payload, payload.getOrderList().get(payload.getOrderList().size()-1).getStartTime());

        Field cardField = test.getClass().getDeclaredField("listHBox");
        cardField.setAccessible(true);
        HBox box = (HBox)cardField.get(test);
        assertEquals(payload.getOrderList().size(), box.getChildren().size());
    }


}