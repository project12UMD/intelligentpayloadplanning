package DataHandlers.Constraint;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Tests the ConstraintBuilder class.
 */
public class ConstraintTest
{
    final int BAD_TIME = -1;
    final int BAD_AMOUNT = 0;
    final int BAD_LIMIT = 0;
    private final int TEST_VALUE = 2;
    Constraint.Builder builder;

    /**
     * Creates a constraint builder for each test.
     */
    @BeforeEach
    public void setup()
    {
        builder = new Constraint.Builder();
    }

    /**
     * Ensures that the limit constraints are set correctly.
     */
    @Test
    public void testSettingLimits()
    {
        builder.setDeploymentLimit(TEST_VALUE)
               .setHatchLimit(TEST_VALUE);
        Constraint constraints = builder.build();
        assertEquals(TEST_VALUE, constraints.getHatchLimit());
        assertEquals(TEST_VALUE, constraints.getDeploymentLimit());
    }

    /**
     * Ensures that the amount constraints are set correctly.
     */
    @Test
    public void testSettingAmounts()
    {
        builder.setTubeAmount(TEST_VALUE)
               .setCellAmount(TEST_VALUE);
        Constraint constraints = builder.build();
        assertEquals(TEST_VALUE, constraints.getCellAmount());
        assertEquals(TEST_VALUE, constraints.getTubeAmount());
    }

    /**
     * Ensures that the time constraints are set correctly.
     */
    @Test
    public void testSettingTime()
    {
        builder.setDeploymentTime(TEST_VALUE)
               .setHatchTime(TEST_VALUE);
        Constraint constraints = builder.build();
        assertEquals(TEST_VALUE, constraints.getHatchTime());
        assertEquals(TEST_VALUE, constraints.getDeploymentTime());
    }

    /**
     * Ensures that the location constraints are set correctly.
     */
    @Test
    public void testSettingLocation()
    {
        final int[] PAYLOAD_AMOUNTS = {5, 10, 15};
        final int[] TRAVEL_TIME = {2, 8, 16};
        final int[] ID = {1, 2, 3};

        for(int i = 0; i < PAYLOAD_AMOUNTS.length; i++)
        {
            builder.addLocation(PAYLOAD_AMOUNTS[i], TRAVEL_TIME[i]);
        }
        Constraint constraints = builder.build();
        List<Location> list = constraints.getLocations();
        assertEquals(ID.length, list.size());
        for(int i = 0; i < ID.length; i++)
        {
            assertEquals(ID[i], list.get(i)
                                    .getId());
            assertEquals(PAYLOAD_AMOUNTS[i], list.get(i)
                                                 .getPayloadAmount());
            assertEquals(TRAVEL_TIME[i], list.get(i)
                                             .getTravelTime());
        }
    }

    /**
     * Ensures an exception is thrown if an invalid location is passed.
     */
    @Test
    public void testInvalidLocationInput()
    {
        try
        {
            builder.addLocation(BAD_AMOUNT, TEST_VALUE);
            fail("Location exception not thrown.");
        }
        catch(IllegalArgumentException e)
        {
        }
        try
        {
            builder.addLocation(TEST_VALUE, BAD_TIME);
            fail("Location exception not thrown.");
        }
        catch(IllegalArgumentException e)
        {
        }
    }

    /**
     * Ensures an exception is thrown if an invalid amount is passed.
     */
    @Test
    public void testInvalidAmountInput()
    {
        try
        {
            builder.setTubeAmount(BAD_AMOUNT);
            fail("Tube Amount exception not thrown.");
        }
        catch(IllegalArgumentException e)
        {
        }
        try
        {
            builder.setCellAmount(BAD_AMOUNT);
            fail("Cell Amount exception not thrown.");
        }
        catch(IllegalArgumentException e)
        {
        }
    }

    /**
     * Ensures that an exception is thrown if an invalid time is passed.
     */
    @Test
    public void testInvalidTimeInput()
    {
        try
        {
            builder.setHatchTime(BAD_TIME);
            fail("Hatch Time exception not thrown.");
        }
        catch(IllegalArgumentException e)
        {
        }
        try
        {
            builder.setDeploymentTime(BAD_TIME);
            fail("Deployment Time exception not thrown.");
        }
        catch(IllegalArgumentException e)
        {
        }
    }

    /**
     * Ensures an exception is thrown if an invalid limit is passed.
     */
    @Test
    public void testInvalidLimitInput()
    {
        try
        {
            builder.setHatchLimit(BAD_LIMIT);
            fail("Hatch Limit exception not thrown.");
        }
        catch(IllegalArgumentException e)
        {
        }
        try
        {
            builder.setDeploymentLimit(BAD_LIMIT);
            fail("Deployment Limit exception not thrown.");
        }
        catch(IllegalArgumentException e)
        {
        }
    }

    /**
     * Tests that the builder starts with the correct default and that it returns to default after it builds
     * an object.
     */
    @Test
    public void testDefault()
    {
        Constraint.Builder builder = new Constraint.Builder();
        testDefaultValues(builder.build());
        builder.setDeploymentTime(TEST_VALUE)
               .setHatchTime(TEST_VALUE)
               .setCellAmount(TEST_VALUE)
               .setTubeAmount(TEST_VALUE)
               .setHatchTime(TEST_VALUE)
               .setDeploymentTime(TEST_VALUE)
               .addLocation(TEST_VALUE, TEST_VALUE)
               .addLocation(TEST_VALUE, TEST_VALUE)
               .build();
        testDefaultValues(builder.build());
    }

    /**
     * Internal class that tests if a given set of constraints is equal to default values.
     *
     * @param input
     *         The constraint class to be tested.
     */
    private void testDefaultValues(Constraint input)
    {
        final int DEFAULT_TIME = 0;
        final int DEFAULT_LIMIT = 1;
        final int DEFAULT_AMOUNT = 1;

        assertEquals(DEFAULT_TIME, input.getDeploymentTime(), "Test deployment time");
        assertEquals(DEFAULT_TIME, input.getHatchTime(), "Test hatch time");
        assertEquals(DEFAULT_LIMIT, input.getDeploymentLimit(), "Test deployment limit");
        assertEquals(DEFAULT_LIMIT, input.getHatchLimit(), "Test hatch limit");
        assertEquals(DEFAULT_AMOUNT, input.getCellAmount(), "Test cell amount");
        assertEquals(DEFAULT_AMOUNT, input.getTubeAmount(), "Test tube amount");
        assertEquals(0, input.getLocations()
                             .size(), "Test size");
    }
}
