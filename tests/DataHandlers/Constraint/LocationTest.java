package DataHandlers.Constraint;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Tests the Location class.
 */
public class LocationTest
{
    private final int BAD_TRAVEL_INPUT = -1;
    private final int BAD_PAYLOAD_INPUT = 0;
    private final int PAYLOAD_AMOUNT = 5;
    private final int TRAVEL_TIME = 10;
    private final int ID = 15;

    /**
     * Ensures that the returned payload amount is correct.
     */
    @Test
    public void testGetPayloadAmount()
    {
        Location test = new Location(ID, PAYLOAD_AMOUNT, TRAVEL_TIME);
        assertEquals(PAYLOAD_AMOUNT, test.getPayloadAmount());
    }

    /**
     * Ensures that the returned travel time is correct.
     */
    @Test
    public void testGetTravelTime()
    {
        Location test = new Location(ID, PAYLOAD_AMOUNT, TRAVEL_TIME);
        assertEquals(TRAVEL_TIME, test.getTravelTime());
    }

    /**
     * Ensures that the returned id is correct.
     */
    @Test
    public void testGetId()
    {
        Location test = new Location(ID, PAYLOAD_AMOUNT, TRAVEL_TIME);
        assertEquals(ID, test.getId());
    }

    /**
     * Ensures exceptions are thrown with invalid inputs.
     */
    @Test
    public void testInvalidInput()
    {
        try
        {
            Location test = new Location(ID, BAD_PAYLOAD_INPUT, TRAVEL_TIME);
            fail("Payload amount exception not thrown.");
        }
        catch(IllegalArgumentException e)
        {
        }

        try
        {
            Location test = new Location(ID, PAYLOAD_AMOUNT, BAD_TRAVEL_INPUT);
            fail("Travel time exception not thrown.");
        }
        catch(IllegalArgumentException e)
        {
        }
    }
}
