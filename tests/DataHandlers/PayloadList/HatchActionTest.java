package DataHandlers.PayloadList;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests that HatchAction works correctly.
 */
public class HatchActionTest
{
    private final HatchAction.TYPE ACTION_TYPE = HatchAction.TYPE.CLOSE;

    /**
     * Ensures that getActionType returns correctly.
     */
    @Test
    public void testGetActionType()
    {
        HatchAction test = new HatchAction(0, 0, ACTION_TYPE);
        assertEquals(ACTION_TYPE, test.getActionType());
    }
}
