package DataHandlers.PayloadList;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Tests the DeploymentAction object which HatchAction and PayloadAction inherits from.
 */
public class DeploymentActionTest
{
    /**
     * Tests getting the tube value.
     */
    @Test
    public void testGetTube()
    {
        final int VALUE = 5;
        DeploymentAction action = new DeploymentAction(VALUE, 0)
        {
        };
        assertEquals(VALUE, action.getTube());
    }

    /**
     * Tests getting the action time value.
     */
    @Test
    public void testGetActionTime()
    {
        final int VALUE = 5;
        DeploymentAction action = new DeploymentAction(0, VALUE)
        {
        };
        assertEquals(VALUE, action.getActionTime());
    }

    /**
     * Tests setting and getting the start time value.
     */
    @Test
    public void testSetGetStartTime()
    {
        final int VALUE = 5;
        DeploymentAction action = new DeploymentAction(0, VALUE)
        {
        };
        action.setStartTime(VALUE);
        assertEquals(VALUE, action.getStartTime());
    }

    /**
     * Ensures that an exception is thrown if one tries to get the start time before setting it.
     */
    @Test
    public void testGetStartTimeException()
    {
        DeploymentAction action = new DeploymentAction(0, 0)
        {
        };
        try
        {
            action.getStartTime();
            fail("IllegalStateException not thrown.");
        }
        catch(IllegalStateException e)
        {
        }
    }


    /**
     * Ensures that an exception is thrown if an invalid input is passed to setStarTime().
     */
    @Test
    public void testSetStartTimeException()
    {
        DeploymentAction action = new DeploymentAction(0, 0)
        {
        };
        try
        {
            action.setStartTime(-1);
            fail("IllegalStateException not thrown.");
        }
        catch(IllegalArgumentException e)
        {
        }
    }
}
