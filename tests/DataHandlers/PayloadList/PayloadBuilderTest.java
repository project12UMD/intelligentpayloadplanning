package DataHandlers.PayloadList;

import DataHandlers.Constraint.Constraint;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests the PayloadListBuilder
 */
public class PayloadBuilderTest
{
    private final int DEPLOYMENT_TIME = 3;
    private final int HATCH_TIME = 3;

    private List<List<DeploymentAction>> expected;
    private PayloadList result;
    private Constraint constraints;
    private int expectedArrival;
    private int expectedAction;

    /**
     * Creates a Constraint object with generic settings.
     */
    @BeforeEach
    public void setup()
    {
        final int HATCH_LIMIT = 2;
        final int DEPLOY_LIMIT = 2;

        Constraint.Builder builder = new Constraint.Builder();
        builder.setDeploymentLimit(DEPLOY_LIMIT)
               .setHatchLimit(HATCH_LIMIT)
               .setDeploymentTime(DEPLOYMENT_TIME)
               .setHatchTime(HATCH_TIME)
               .setCellAmount(5)
               .setTubeAmount(5)
               .addLocation(4, 3);
        expected = new ArrayList<>();
        constraints = builder.build();
        for(int i = 0; i < HATCH_LIMIT; i++)
        {
            expected.add(new ArrayList<>());
        }
    }

    /**
     * Tests the expected list against the result PayloadList.
     */
    @AfterEach
    public void postTest()
    {
        expected.size();
        result.getSequenceList()
              .size();
        assertEquals(expected.size(), result.getSequenceList()
                                            .size(), "Incorrect Amount of Sequences.");
        for(int i = 0; i < expected.size(); i++)
        {
            assertEquals(expected.get(i)
                                 .size(), result.getSequence(i)
                                                .getUnmodifiableList()
                                                .size(), "Incorrect Sequence Size. Sequence: " + i);
            for(int j = 0; j < expected.get(i)
                                       .size(); j++)
            {
                DeploymentAction subject = result.getSequence(i)
                                                 .getUnmodifiableList()
                                                 .get(j);
                assertTrue(subject.getClass()
                                  .equals(expected.get(i)
                                                  .get(j)
                                                  .getClass()), "Incorrect Class. Sequence: " + i + " Index: " + j);
                assertEquals(expected.get(i)
                                     .get(j)
                                     .getStartTime(), subject.getStartTime(), "Incorrect Start Time. Sequence: " + i
                        + " Index: " + j);
                assertEquals(expected.get(i)
                                     .get(j)
                                     .getActionTime(), subject.getActionTime(), "Incorrect Action Time. Sequence: " +
                        i + " Index: " + j);
            }
        }
        assertEquals(expectedAction, result.getActionTime(), "Action Time Incorrect");
        assertEquals(expectedArrival, result.getArrivalTime(), "Arrival Time Incorrect");
    }

    /**
     * Ensures that when nothing is deployed that the list is empty.
     */
    @Test
    public void testEmpty()
    {
        PayloadListBuilder payloadListBuilder = new PayloadListBuilder(constraints);
        result = payloadListBuilder.build();
    }

    /**
     * Ensures that the builder correctly creates a list with a single deployment.
     */
    @Test
    public void singleDeploy()
    {
        expectedAction = 9;
        expectedArrival = 9;
        PayloadListBuilder payloadListBuilder = new PayloadListBuilder(constraints);
        payloadListBuilder.sequence(0)
                          .tube(0)
                          .deploy(0, constraints.getLocations()
                                                .get(0));
        result = payloadListBuilder.build();
        expected.get(0)
                .add(new HatchAction(0, HATCH_TIME, HatchAction.TYPE.OPEN));
        expected.get(0)
                .add(new PayloadAction(0, DEPLOYMENT_TIME, 0, constraints.getLocations()
                                                                         .get(0)));
        expected.get(0)
                .add(new HatchAction(0, HATCH_TIME, HatchAction.TYPE.CLOSE));
        expected.get(0)
                .get(0)
                .setStartTime(0);
        expected.get(0)
                .get(1)
                .setStartTime(3);
        expected.get(0)
                .get(2)
                .setStartTime(6);
    }

    /**
     * Tests that simultaneous deployments work correctly.
     */
    @Test
    public void simDeployments()
    {
        expectedAction = 9;
        expectedArrival = 9;
        PayloadListBuilder payloadListBuilder = new PayloadListBuilder(constraints);
        payloadListBuilder.sequence(0)
                          .tube(0)
                          .deploy(0, constraints.getLocations()
                                                .get(0))
                          .deploy(1, constraints.getLocations()
                                                .get(0))
                          .sequence(1)
                          .tube(1)
                          .deploy(0, constraints.getLocations()
                                                .get(0))
                          .deploy(1, constraints.getLocations()
                                                .get(0));
        result = payloadListBuilder.build();
        expected.get(0)
                .add(new HatchAction(0, HATCH_TIME, HatchAction.TYPE.OPEN));
        expected.get(0)
                .add(new PayloadAction(0, DEPLOYMENT_TIME, 0, constraints.getLocations()
                                                                         .get(0)));
        expected.get(0)
                .add(new PayloadAction(0, DEPLOYMENT_TIME, 1, constraints.getLocations()
                                                                         .get(0)));
        expected.get(0)
                .add(new HatchAction(0, HATCH_TIME, HatchAction.TYPE.CLOSE));
        expected.get(1)
                .add(new HatchAction(1, HATCH_TIME, HatchAction.TYPE.OPEN));
        expected.get(1)
                .add(new PayloadAction(1, DEPLOYMENT_TIME, 0, constraints.getLocations()
                                                                         .get(0)));
        expected.get(1)
                .add(new PayloadAction(1, DEPLOYMENT_TIME, 1, constraints.getLocations()
                                                                         .get(0)));
        expected.get(1)
                .add(new HatchAction(1, HATCH_TIME, HatchAction.TYPE.CLOSE));
        expected.get(0)
                .get(0)
                .setStartTime(0);
        expected.get(0)
                .get(1)
                .setStartTime(3);
        expected.get(0)
                .get(2)
                .setStartTime(3);
        expected.get(0)
                .get(3)
                .setStartTime(6);
        expected.get(1)
                .get(0)
                .setStartTime(0);
        expected.get(1)
                .get(1)
                .setStartTime(3);
        expected.get(1)
                .get(2)
                .setStartTime(3);
        expected.get(1)
                .get(3)
                .setStartTime(6);
    }
}
