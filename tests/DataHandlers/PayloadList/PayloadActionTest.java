package DataHandlers.PayloadList;

import DataHandlers.Constraint.Location;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests the PayloadAction class.
 */
public class PayloadActionTest
{
    /**
     * Ensures that the correct location is returned.
     */
    @Test
    public void testGetTarget()
    {
        final int ID = 5;
        final int PAYLOADS = 10;
        final int TIME = 15;
        Location target = new Location(ID, PAYLOADS, TIME);
        PayloadAction action = new PayloadAction(0, 0, 0, target);
        assertEquals(target, action.getTarget());
    }

    /**
     * Ensures that the correct value of deploy is returned.
     */
    @Test
    public void testGetCell()
    {
        final int CELL = 5;
        PayloadAction action = new PayloadAction(0, 0, CELL, null);
        assertEquals(CELL, action.getCell());
    }

    /**
     * Ensures that the arrival time is calculated correctly.
     */
    @Test
    public void testGetArrivalTime()
    {
        final int TRAVEL_TIME = 10;
        final int START_TIME = 5;
        final int ACTION_TIME = 15;
        final int ARRIVAL_TIME = TRAVEL_TIME + START_TIME + ACTION_TIME;

        Location target = new Location(0, 1, TRAVEL_TIME);
        PayloadAction action = new PayloadAction(0, ACTION_TIME, 0, target);
        action.setStartTime(START_TIME);
        assertEquals(ARRIVAL_TIME, action.getArrivalTime());
    }
}
