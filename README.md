5   # README #

Intelligent Payload Planning Tool
University of Massachusetts Dartmouth
Minimum Viable Product
Version 0.1

Authors:
Brian Towne
Gurvinder Singh
Lucas Yosimura
Evan Garcia
Soravath Mengseng

Client:
Christopher Lalibe
Naval Undersea Warfare Center

This is the minimum viable product for the Intelligent Payload Planning Tool. This is an operational demo that is to be used to help direct further development of the project. Below is a number of notes regarding further areas of developement

Modifications
-The card panel is not visually ideal and looks cluttered. Work needs to be done to improve the look of the cards to make them easier to understand.
-Invalid input is currently handled by alert boxes. More effective and less obnoxious methods of input validation should be used.
-The optimizer currently used does not optimize correctly across the entire input domain. More work needs to be put into the optimizer to ensure that it is correct across the entire input domain.
-JavaDocs need to be completed and tidied up for all classes and methods.
-Unit tests need to be completed for non GUI code. All GUI code will be tested via manual testing methods. Manual tests for this code needs to be created.

Features
-The card panel does not effectively demonstrate the parallelism that is happening in the execution of the list. A different chart, possibly gant, should be used to demonstrate this. The user should be able to switch between the charts.
-The list should be simulatable allowing the user to see how the list would run in real time.
-Faults/used cells should be selectable as input. The optimizer then would not select the cells as it created the list. 
-Importing/exporting of lists/constraints.